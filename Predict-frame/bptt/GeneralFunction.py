import tensorflow as tf
import numpy as np
def selu(x):
    return tf.nn.relu(x)
    alpha = 1.6732632423543772848170429916717
    scale = 1.0507009873554804934193349852946
    return scale*tf.where(x>=0.0, x, alpha*tf.nn.elu(x))

def combined_loss(gen_frames, gt_frames):
    lam_lp = 1
    lam_gdl = 0

    loss = lam_lp * lp_loss(gen_frames, gt_frames)
    loss += lam_gdl * gdl_loss(gen_frames, gt_frames, 2)
    return loss

def lp_loss(gen_frames, gt_frames):
    squared_deltas = tf.square(gen_frames - gt_frames)
    return tf.reduce_mean(squared_deltas)


def gdl_loss(gen_frames, gt_frames, alpha):
    pos = tf.constant(np.identity(1), dtype=tf.float32)
    neg = -1 * pos
    filter_x = tf.expand_dims(tf.stack([neg, pos]), 0)  # [-1, 1]
    filter_y = tf.stack([tf.expand_dims(pos, 0), tf.expand_dims(neg, 0)])  # [[1],[-1]]
    strides = [1, 1, 1, 1]  # stride of (1, 1)
    padding = 'SAME'

    gen_dx = tf.abs(tf.nn.conv2d(gen_frames, filter_x, strides, padding=padding))
    gen_dy = tf.abs(tf.nn.conv2d(gen_frames, filter_y, strides, padding=padding))
    gt_dx = tf.abs(tf.nn.conv2d(gt_frames, filter_x, strides, padding=padding))
    gt_dy = tf.abs(tf.nn.conv2d(gt_frames, filter_y, strides, padding=padding))

    grad_diff_x = tf.abs(gt_dx - gen_dx)
    grad_diff_y = tf.abs(gt_dy - gen_dy)

    return tf.reduce_mean((grad_diff_x ** alpha + grad_diff_y ** alpha))
