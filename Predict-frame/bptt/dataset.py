import numpy as np
import bptt.constant as constant
import random
import os
from scipy import misc
from PIL import Image

class Dataset:
    mean=None # Mean of DatasetImages
    def __init__(self):
        self.folderlist = os.listdir(constant.DatasetAddress)
        self.mean = np.load(constant.MeanFileAddress)
        return
    def GetMean(self):
        return self.mean
    def CreateMiniBatch(self, size,k):
        counter = 0
        imglist = np.zeros((size, constant.input_height, constant.input_width, constant.historyLength))
        outputlist = np.zeros((size, constant.input_height, constant.input_width, 1))
        actionList = np.zeros((size, constant.number_of_action))
        while counter < size:
            randomfolder, randomfile = self.GetRandomEpisodeFrame()
            actionList[counter, :] = self.GetAction(randomfolder, randomfile)
            imglist[counter , :, :, 3] = self.GetImage(randomfolder, randomfile)
            imglist[counter , :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
            imglist[counter , :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
            imglist[counter , :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
            outputlist[counter , :, :, 0] = self.GetImage(randomfolder, randomfile + k)

            counter = counter + 1
        return imglist, outputlist, actionList

    def GetRandomEpisodeFrame(self):
        while True:
            episode = random.choice(self.folderlist)
            filelist = os.listdir(os.path.join(constant.DatasetAddress, episode))
            if (len(filelist) < 25):
                continue
            frame = random.randrange(6, len(filelist) - 6)
            return episode, frame

    def GetAction(self, episode, frame, fullAction=False):
        actionListfile = open(os.path.join(os.path.join(constant.DatasetAddress, episode), "act.log"))
        if fullAction == True:
            line = actionListfile.readlines()
            actionListindex = [int(i) for i in line]
            del actionListindex[0:frame]
            actionList = np.zeros((len(actionListindex), constant.number_of_action))
            for i in range(len(actionListindex)):
                actionList[i, actionListindex[i]] = 1
        else:
            actionList = np.zeros((1, constant.number_of_action))
            ind = int(actionListfile.readlines()[frame])
            actionList[0, ind] = 1
        actionListfile.close()
        return actionList
    def GetImage(self, episode, frame):
        return (misc.imread(os.path.join(os.path.join(constant.DatasetAddress, episode),
                                             '{:05d}'.format(frame) + ".png")) - self.GetMean()) / 255

    def GetRandomImage(self, fullAction=False):
        randomfolder, randomfile = self.GetRandomEpisodeFrame()

        imglist = np.zeros((1, constant.input_height, constant.input_width, constant.historyLength))
        imglist[0, :, :, 3] = self.GetImage(randomfolder, randomfile)
        imglist[0, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
        imglist[0, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
        imglist[0, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
        actionList = self.GetAction(randomfolder, randomfile, fullAction)
        outputlist = self.GetImage(randomfolder, randomfile + 1)
        return imglist, actionList, outputlist, randomfolder, randomfile

    def ResizeImage(self, img):
        return misc.imresize(img, 2.0)

    def CreateOutput(self, folder, file):
        imagesize = constant.input_width * 2
        filelist = sorted(os.listdir(constant.OutputAddress))
        count = 0
        for filename in filelist:
            if filename == "act.log":
                continue
            outfile = np.zeros(shape=(imagesize, imagesize * 2))
            out = self.ResizeImage(misc.imread(os.path.join(constant.OutputAddress, "out" + str(count) + ".png")))
            out = Image.fromarray(out).convert("L")
            infile = self.ResizeImage(misc.imread(
                os.path.join(os.path.join(constant.DatasetAddress, folder), '{:05d}'.format(file + count+constant.historyLength+1) + ".png")))
            outfile[:, :imagesize] = out
            outfile[:, imagesize:] = infile
            Image.fromarray(outfile).convert('RGB').save("output/zout" + str(count) + ".png")
            os.remove(os.path.join(constant.OutputAddress, "out" + str(count) + ".png"))
            count += 1