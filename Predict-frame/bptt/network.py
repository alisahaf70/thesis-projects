import bptt.generator
import bptt.GeneralFunction
import bptt.constant as constant
import tensorflow as tf
import shutil
import os
import numpy as np
from PIL import Image
import re

LearningRate = tf.Variable(1e-4,trainable=False)
global_step = tf.get_variable('global_step', shape=(), initializer=tf.constant_initializer(0.0),
                                   trainable=False)
learning_rate = tf.train.exponential_decay(LearningRate, global_step, 1e5, 0.9, staircase=True)
optimizer = tf.train.AdamOptimizer(LearningRate)

class network:
    def __init__(self,k,dataset,session):
        #Create PlaceHolder Variables
        self.dataset=dataset
        self.sess=session
        self.action = tf.placeholder(dtype=tf.float32, shape=(None, constant.number_of_action),name="networkAction")
        self.x = tf.placeholder(dtype=tf.float32, shape=(None, constant.input_height, constant.input_width, constant.historyLength),name="networkinput")
        self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, constant.input_height, constant.input_width, 1),name="networklabel")
        self.k=k
        if k==1:
            self.CreateNetworkforKEqualOne()
        elif k==3:
            self.CreateNetworkforKEqualThree()
        elif k==5:
            self.CreateNetworkforKEqualFive()

    def CreateNetworkforKEqualOne(self):
        generator1=bptt.generator.Generator(1)
        generator1net=generator1.Create_Network(self.action,self.x)
        self.out=generator1net
        return self.CreateOptimiser(generator1net,1e-4)

    def CreateNetworkforKEqualThree(self):
        generator1=bptt.generator.Generator(3)
        generator2=bptt.generator.Generator(3)
        generator3=bptt.generator.Generator(3)

        generator1net=generator1.Create_Network(self.action,self.x)
        self.out = generator1net
        input2=tf.concat((self.x[:,:,:,1:4],generator1net),axis=3)
        generator2net=generator2.Create_Network(self.action,input2)

        input3 = tf.concat((input2[:, :, :, 1:4], generator2net), axis=3)
        generator3net = generator3.Create_Network(self.action, input3)

        return self.CreateOptimiser(generator3net,1e-5)

    def CreateNetworkforKEqualFive(self):
        generator1 = bptt.generator.Generator(5)
        generator2 = bptt.generator.Generator(5)
        generator3 = bptt.generator.Generator(5)
        generator4 = bptt.generator.Generator(5)
        generator5 = bptt.generator.Generator(5)

        generator1net = generator1.Create_Network(self.action, self.x)
        self.out = generator1net
        input2 = tf.concat((self.x[:, :, :, 1:4], generator1net), axis=3)
        generator2net = generator2.Create_Network(self.action, input2)

        input3 = tf.concat((input2[:, :, :, 1:4], generator2net), axis=3)
        generator3net = generator3.Create_Network(self.action, input3)

        input4 = tf.concat((input3[:, :, :, 1:4], generator3net), axis=3)
        generator4net = generator4.Create_Network(self.action, input4)

        input5 = tf.concat((input4[:, :, :, 1:4], generator4net), axis=3)
        generator5net = generator5.Create_Network(self.action, input5)

        return self.CreateOptimiser(generator5net, 1e-5)

    def CreateOptimiser(self,out,LR):
        penalty = tf.reduce_sum(0 * tf.stack([tf.nn.l2_loss(var) for var in tf.trainable_variables()]),
                                name='regularization')
        self.loss= bptt.GeneralFunction.combined_loss(out, self.y_)+penalty



        grads_vars = optimizer.compute_gradients(self.loss)
        bias_pattern = re.compile('.*bias*')
        grads_vars_mult = []
        for grad, var in grads_vars:
            if bias_pattern.match(var.op.name):
                grads_vars_mult.append((grad * 2.0, var))
            else:
                grads_vars_mult.append((grad, var))

        grads_clip = [(tf.clip_by_value(grad, -0.1, 0.1), var) for grad, var in grads_vars_mult]
        self.train = optimizer.apply_gradients(grads_clip, global_step=global_step)
        return self.train

    def PredictImageSequence(self, img, actionList, step):
        step=min(200,step)
        shutil.rmtree('output')
        os.mkdir('output')
        imglist = np.zeros((constant.input_height, constant.input_width, step + constant.historyLength))
        imglist[:, :, 0:constant.historyLength] = img

        for i in range(step - constant.historyLength):
            imglist[:, :, constant.historyLength + i] = np.reshape(self.sess.run(self.out, feed_dict={self.x: [imglist[:, :, i:i + constant.historyLength]],
                                                                                                      self.action: [actionList[i + constant.historyLength, :]]}),
                                                                   newshape=(constant.input_height, constant.input_width))
            Image.fromarray((imglist[:, :, constant.historyLength + i] +  self.dataset.GetMean()/ 255) * 255).convert('RGB').save(
                "output/out" + str(i) + ".png")
        outfile = open("output/act.log", "w+")
        ac = np.argmax(actionList, axis=1).tolist()
        outfile.writelines(list("%d\n" % item for item in ac))