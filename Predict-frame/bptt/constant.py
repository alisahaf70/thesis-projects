

DatasetAddress="/home/alireza/Projects/thesis-projects/Predict-frame/datasets/pong-2/"
OutputAddress="/home/alireza/Projects/thesis-projects/Predict-frame/output/"
MeanFileAddress="/home/alireza/Projects/thesis-projects/Predict-frame/datasets/pong2.npy"
numberofTrainningEpoch=3.5e5
k1to3Switch=1.5e5
k3to5switch=2.5e5
firstbatchsize=32
secondbatchsize=8
input_height=84
input_width=84
historyLength=4
cnn_format="NHWC"
number_of_action=6

