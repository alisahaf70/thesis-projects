import tensorflow as tf
from bptt.GeneralFunction import *
import bptt.constant as constant
class Generator:
    w = {}
    def __init__(self,k):
        self.k=k
    def Create_Network(self,action,inputimage):
        initializer = tf.contrib.layers.xavier_initializer_conv2d(uniform=True)
        LinearInitializer = tf.contrib.layers.xavier_initializer(uniform=True)
        BiasInitializer = tf.constant_initializer(0.0)

        #We Must Use Same Weight for all network
        if self.k==1:
            usePrevWeight=False
        else:
            usePrevWeight=True



        with tf.variable_scope("G",reuse=usePrevWeight) as scope:
            self.w["Wconv1"] = tf.get_variable("Wconv1", shape=[6, 6, constant.historyLength, 64], initializer=initializer)
            conv1 = tf.nn.conv2d(inputimage, filter=self.w["Wconv1"], strides=[1, 2, 2, 1], data_format=constant.cnn_format,
                                 padding="VALID", name="conv1")
            relu_conv1 = selu(
                tf.nn.bias_add(conv1, tf.get_variable('conv1biases', [64], initializer=BiasInitializer), constant.cnn_format))

            self.w["Wconv2"] = tf.get_variable("Wconv2", shape=[6, 6, relu_conv1.get_shape()[-1], 64],
                                               initializer=initializer)
            conv2 = tf.nn.conv2d(relu_conv1, self.w["Wconv2"], strides=[1, 2, 2, 1], data_format=constant.cnn_format,
                                 padding="SAME", name="conv2")
            relu_conv2 = selu(
                tf.nn.bias_add(conv2, tf.get_variable('conv2biases', [64], initializer=BiasInitializer), constant.cnn_format))

            self.w["Wconv3"] = tf.get_variable("Wconv3", shape=[6, 6, relu_conv2.get_shape()[-1], 64],
                                               initializer=initializer)
            conv3 = tf.nn.conv2d(relu_conv2, self.w["Wconv3"], strides=[1, 2, 2, 1], data_format=constant.cnn_format,
                                 padding="SAME", name="conv3")
            relu_conv3 = selu(
                tf.nn.bias_add(conv3, tf.get_variable('conv3biases', [64], initializer=BiasInitializer), constant.cnn_format))
            conv3_flat = tf.reshape(relu_conv3, shape=[-1, 10 * 10 * 64], name="l3flat")

            self.w["Wflat1"] = tf.get_variable("WFlat1", shape=[10 * 10 * 64, 1024], initializer=LinearInitializer)
            l1flat = selu(tf.nn.bias_add(tf.matmul(conv3_flat, self.w["Wflat1"]),
                                               tf.get_variable('flat1biases', [1024], initializer=BiasInitializer)))

            self.w["Wflat2"] = tf.get_variable("WFlat2", shape=[1024, 2048], initializer=tf.random_uniform_initializer(minval=-1.0, maxval=1.0))
            l2flat = tf.nn.bias_add(tf.matmul(l1flat, self.w["Wflat2"]),
                                    tf.get_variable('flat2biases', [2048], initializer=BiasInitializer))


            self.w["WActionflat"] = tf.get_variable("WActionflat", shape=[constant.number_of_action, 2048],
                                                    initializer=tf.random_uniform_initializer(minval=-0.1, maxval=0.1))
            actionflat = tf.nn.bias_add(tf.matmul(action, self.w["WActionflat"]),
                                        tf.get_variable('actionflatbias', [2048], initializer=BiasInitializer))

            transformation = tf.multiply(actionflat, l2flat, "Combine_Layer")

            self.w["Wflat3"] = tf.get_variable("Wflat3", shape=[2048, 1024], initializer=LinearInitializer)
            l3flat = tf.nn.bias_add(tf.matmul(transformation, self.w["Wflat3"]),
                                    tf.get_variable('flat3biases', [1024], initializer=BiasInitializer))

            self.w["Wflat5"] = tf.get_variable("Wflat5", shape=[1024, 64 * 10 * 10], initializer=LinearInitializer)
            l5flat = selu(tf.nn.bias_add(selu(tf.matmul(l3flat, self.w["Wflat5"])),
                                    tf.get_variable('flat5biases', [64 * 10 * 10], initializer=BiasInitializer)))

            l5 = tf.reshape(l5flat, (-1, 10, 10, 64),name="l6flat")

            self.w["WDeconv1"] = tf.get_variable("WDeconv1", shape=(6, 6, 64, 64), initializer=initializer)
            deconv1 = selu(tf.nn.bias_add(tf.nn.conv2d_transpose(l5, self.w["WDeconv1"], strides=[1, 2, 2, 1],
                                                                       output_shape=(tf.shape(inputimage)[0], 20, 20, 64),
                                                                       padding="SAME", data_format=constant.cnn_format,
                                                                       name="deconv1"),
                                                tf.get_variable('deconv1biases', [64], initializer=BiasInitializer)))

            self.w["WDeconv2"] = tf.get_variable("WDeconv2", shape=(6, 6, 64, 64), initializer=initializer)
            deconv2 = selu(tf.nn.bias_add(tf.nn.conv2d_transpose(deconv1, self.w["WDeconv2"], strides=[1, 2, 2, 1],
                                                                       output_shape=(tf.shape(inputimage)[0], 40, 40, 64),
                                                                       padding="SAME", data_format=constant.cnn_format,
                                                                       name="deconv2"),
                                                tf.get_variable('deconv2biases', [64], initializer=BiasInitializer)))

            self.w["WDeconv3"] = tf.get_variable("WDeconv3", shape=(6, 6, 1, 64), initializer=initializer)
            self.deconv3 = tf.nn.bias_add(tf.nn.conv2d_transpose(deconv2, self.w["WDeconv3"], strides=[1, 2, 2, 1],
                                                                 output_shape=(
                                                                 tf.shape(inputimage)[0], constant.input_height, constant.input_width,
                                                                 1), padding="VALID", data_format=constant.cnn_format,
                                                                 name="deconv3"),
                                          tf.get_variable('deconv3biases', [1], initializer=BiasInitializer))
            self.output = self.deconv3
            return self.output