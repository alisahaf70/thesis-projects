import tensorflow as tf
import os
import random
import numpy as np
from scipy import misc
from PIL import Image
import pickle


class Frame_predict():
    # resnet
    DataSetaddress = "/home/mll/@li/thesis-projects/Predict-frame/image/"  # "/home/alireza/Projects/thesis-projects/utility/ImageDataset/img"
    historyLength = 4
    input_width = 84
    input_height = 84
    Read_image_file = True
    k = 1
    number_of_action = 6
    cnn_format = "NHWC"  # NHWC
    w = {}

    def ResizeImage(self, img):
        return misc.imresize(img, 2.0)

    def CreateOutput(self, folder, file):
        imagesize = 84 * 2
        filelist = sorted(os.listdir("/home/mll/@li/thesis-projects/Predict-frame/output/"))
        count = 0
        for filename in filelist:
            if filename == "act.log":
                continue
            outfile = np.zeros(shape=(imagesize, imagesize * 2))
            out = self.ResizeImage(misc.imread(
                os.path.join("/home/mll/@li/thesis-projects/Predict-frame/output/", "out" + str(count) + ".png")))
            out = Image.fromarray(out).convert("L")
            infile = self.ResizeImage(misc.imread(os.path.join(os.path.join(self.DataSetaddress, randomfolder),
                                                               '{:05d}'.format(randomfile + count+self.historyLength+1) + ".png")))
            outfile[:, :imagesize] = out
            outfile[:, imagesize:] = infile
            Image.fromarray(outfile).convert('RGB').save("output/zout" + str(count) + ".png")
            os.remove(os.path.join("/home/mll/@li/thesis-projects/Predict-frame/output/", "out" + str(count) + ".png"))
            count += 1

    def __init__(self):
        print("class created")
        self.Create_Network()
        self.CreateOptimiser()
        self.sess = tf.Session()
        self.saver = tf.train.Saver()

        if self.Read_image_file:
            self.folderlist = os.listdir(self.DataSetaddress)
        tf.summary.scalar("Loss", self.lossMSE)
        self.merge = tf.summary.merge_all()
        self.writer = tf.summary.FileWriter("Log/")

        self.sess.run(tf.global_variables_initializer())
        if (os.path.isfile("save/model.ckpt.index")):
            self.saver.restore(self.sess, "save/model.ckpt")

    def CreateOptimiser(self):
        self.LearningRate = tf.Variable(1e-4)
        squared_deltas = tf.square(self.y_ - self.output)
        self.lossMSE = tf.reduce_sum(squared_deltas)
        self.loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=self.deconv3, logits=self.y_)#+self.gdl_loss(self.output,self.y_,2)
        optimizer = tf.train.AdamOptimizer(self.LearningRate, 0.9, 0.95)
        self.train = optimizer.minimize(self.lossMSE)

    def gdl_loss(self,gen_frames, gt_frames, alpha):
        pos = tf.constant(np.identity(1), dtype=tf.float32)
        neg = -1 * pos
        filter_x = tf.expand_dims(tf.stack([neg, pos]), 0)  # [-1, 1]
        filter_y = tf.stack([tf.expand_dims(pos, 0), tf.expand_dims(neg, 0)])  # [[1],[-1]]
        strides = [1, 1, 1, 1]  # stride of (1, 1)
        padding = 'SAME'

        gen_dx = tf.abs(tf.nn.conv2d(gen_frames, filter_x, strides, padding=padding))
        gen_dy = tf.abs(tf.nn.conv2d(gen_frames, filter_y, strides, padding=padding))
        gt_dx = tf.abs(tf.nn.conv2d(gt_frames, filter_x, strides, padding=padding))
        gt_dy = tf.abs(tf.nn.conv2d(gt_frames, filter_y, strides, padding=padding))

        grad_diff_x = tf.abs(gt_dx - gen_dx)
        grad_diff_y = tf.abs(gt_dy - gen_dy)

        return tf.reduce_sum((grad_diff_x ** alpha + grad_diff_y ** alpha))

    def Create_Network(self):
        initializer = tf.contrib.layers.xavier_initializer_conv2d()
        LinearInitializer = tf.contrib.layers.xavier_initializer()
        BiasInitializer = tf.constant_initializer(0.0)
        if self.Read_image_file == False:
            with open('images', 'rb') as fp:
                self.imagelist = pickle.load(fp, encoding='latin1')
            with open('actions', 'rb') as fp:
                self.actionlist = pickle.load(fp, encoding='latin1')
        self.average = np.load("mean.npy")
        # self.ShowImage(self.average)
        self.action = tf.placeholder(dtype=tf.float32, shape=(None, self.number_of_action))

        self.x = tf.placeholder(dtype=tf.float32, shape=(None, self.input_height, self.input_width, self.historyLength))
        self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, self.input_height, self.input_width, 1))
        self.w["Wconv1"] = tf.get_variable("Wconv1", shape=[6, 6, self.historyLength, 64], initializer=initializer)
        conv1 = tf.nn.conv2d(self.x, filter=self.w["Wconv1"], strides=[1, 2, 2, 1], data_format=self.cnn_format,
                             padding="VALID", name="conv1")
        relu_conv1 = tf.nn.relu(
            tf.nn.bias_add(conv1, tf.get_variable('conv1biases', [64], initializer=BiasInitializer), self.cnn_format))

        self.w["Wconv2"] = tf.get_variable("Wconv2", shape=[6, 6, relu_conv1.get_shape()[-1], 64],
                                           initializer=initializer)
        conv2 = tf.nn.conv2d(relu_conv1, self.w["Wconv2"], strides=[1, 2, 2, 1], data_format=self.cnn_format,
                             padding="SAME", name="conv2")
        relu_conv2 = tf.nn.relu(
            tf.nn.bias_add(conv2, tf.get_variable('conv2biases', [64], initializer=BiasInitializer), self.cnn_format))

        self.w["Wconv3"] = tf.get_variable("Wconv3", shape=[6, 6, relu_conv2.get_shape()[-1], 64],
                                           initializer=initializer)
        conv3 = tf.nn.conv2d(relu_conv2, self.w["Wconv3"], strides=[1, 2, 2, 1], data_format=self.cnn_format,
                             padding="SAME", name="conv3")
        relu_conv3 = tf.nn.relu(
            tf.nn.bias_add(conv3, tf.get_variable('conv3biases', [64], initializer=BiasInitializer), self.cnn_format))
        conv3_flat = tf.reshape(relu_conv3, shape=[-1, 10 * 10 * 64], name="l3flat")

        self.w["Wflat1"] = tf.get_variable("WFlat1", shape=[10 * 10 * 64, 1024], initializer=LinearInitializer)
        l1flat = tf.nn.relu(tf.nn.bias_add(tf.matmul(conv3_flat, self.w["Wflat1"]),
                                           tf.get_variable('flat1biases', [1024], initializer=BiasInitializer)))

        self.w["Wflat2"] = tf.get_variable("WFlat2", shape=[1024, 2048], initializer=LinearInitializer)
        l2flat = tf.nn.bias_add(tf.matmul(l1flat, self.w["Wflat2"]),
                                tf.get_variable('flat2biases', [2048], initializer=BiasInitializer))

        self.w["Wflattemp"] = tf.get_variable("Wflattemp", shape=[2048, 4096],
                                              initializer=tf.random_uniform_initializer(-1, 1))
        ltempflat = tf.nn.bias_add(tf.matmul(l2flat, self.w["Wflattemp"]),
                                   tf.get_variable('flattempbiases', [4096], initializer=BiasInitializer))

        self.w["WActionflat"] = tf.get_variable("WActionflat", shape=[self.number_of_action, 4096],
                                                initializer=tf.random_uniform_initializer(-0.1, 0.1))
        actionflat = tf.nn.bias_add(tf.matmul(self.action, self.w["WActionflat"]),
                                    tf.get_variable('actionflatbias', [4096], initializer=BiasInitializer))

        transformation = tf.multiply(actionflat, ltempflat, "Combine_Layer")

        self.w["Wflat3"] = tf.get_variable("Wflat3", shape=[4096, 2048], initializer=LinearInitializer)
        l3flat = tf.nn.bias_add(tf.matmul(transformation, self.w["Wflat3"]),
                                tf.get_variable('flat3biases', [2048], initializer=BiasInitializer))

        self.w["Wflat4"] = tf.get_variable("Wflat4", shape=[2048, 1024], initializer=LinearInitializer)
        l4flat = tf.nn.bias_add(tf.nn.relu(tf.matmul(l3flat, self.w["Wflat4"])),
                                tf.get_variable('flat4biases', [1024], initializer=BiasInitializer))

        self.w["Wflat5"] = tf.get_variable("Wflat5", shape=[1024, 64 * 10 * 10], initializer=LinearInitializer)
        l5flat = tf.nn.bias_add(tf.nn.relu(tf.matmul(l4flat, self.w["Wflat5"])),
                                tf.get_variable('flat5biases', [64 * 10 * 10], initializer=BiasInitializer))

        l5 = tf.reshape(l5flat, (-1, 10, 10, 64))

        self.w["WDeconv1"] = tf.get_variable("WDeconv1", shape=(6, 6, 64, 64), initializer=initializer)
        deconv1 = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d_transpose(l5, self.w["WDeconv1"], strides=[1, 2, 2, 1],
                                                                   output_shape=(tf.shape(self.x)[0], 20, 20, 64),
                                                                   padding="SAME", data_format=self.cnn_format,
                                                                   name="deconv1"),
                                            tf.get_variable('deconv1biases', [64], initializer=BiasInitializer)))

        self.w["WDeconv2"] = tf.get_variable("WDeconv2", shape=(6, 6, 64, 64), initializer=initializer)
        deconv2 = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d_transpose(deconv1, self.w["WDeconv2"], strides=[1, 2, 2, 1],
                                                                   output_shape=(tf.shape(self.x)[0], 40, 40, 64),
                                                                   padding="SAME", data_format=self.cnn_format,
                                                                   name="deconv2"),
                                            tf.get_variable('deconv2biases', [64], initializer=BiasInitializer)))

        self.w["WDeconv3"] = tf.get_variable("WDeconv3", shape=(6, 6, 1, 64), initializer=initializer)
        self.deconv3 = tf.nn.bias_add(tf.nn.conv2d_transpose(deconv2, self.w["WDeconv3"], strides=[1, 2, 2, 1],
                                                             output_shape=(
                                                             tf.shape(self.x)[0], self.input_height, self.input_width,
                                                             1), padding="VALID", data_format=self.cnn_format,
                                                             name="deconv3"),
                                      tf.get_variable('deconv3biases', [1], initializer=BiasInitializer))
        self.output = tf.nn.tanh(self.deconv3)

    def StartTraining(self):
        batchsize = 32
        self.k = 1
        for i in range(25000):
            imglist, outputlist, actionList = self.CreateMiniBatch(batchsize, self.k)
            # imgg=imglist[0, :, :, 0]
            # self.ShowImage(imgg*255+self.average)
            self.sess.run(self.train, feed_dict={self.x: imglist, self.y_: outputlist, self.action: actionList})

            if (i % 100 == 0):
                loss, merge = self.sess.run([self.lossMSE, self.merge],
                                            feed_dict={self.x: imglist, self.y_: outputlist, self.action: actionList})
                print("Error of Iteration " + str(i) + " is " + str(loss))
                self.writer.add_summary(merge, i)
            if i % 1000 == 0 and i > 1:
                self.saver.save(self.sess, "save/model.ckpt")
            if i == 7000:
                batchsize = 8
                self.k = 3
                self.sess.run(tf.assign(self.LearningRate, 1e-5))
            elif i == 15000:
                self.k = 5

    def GetRandomEpisodeFrame(self):
        while True:
            if self.Read_image_file == True:
                episode = random.choice(self.folderlist)
                filelist = os.listdir(os.path.join(self.DataSetaddress, episode))
                if (len(filelist) < 3 + self.k + 2):
                    continue
                frame = random.randrange(3, len(filelist) - 1 - self.k)
            else:
                episode = random.randrange(0, len(self.imagelist))
                if (len(self.imagelist[episode]) < 3 + self.k + 2):
                    continue
                frame = random.randrange(3, len(self.imagelist[episode]) - 1 - self.k)
            return episode, frame

    def GetRandomImage(self, fullAction=False):
        randomfolder, randomfile = self.GetRandomEpisodeFrame()

        imglist = np.zeros((1, self.input_height, self.input_width, self.historyLength))
        imglist[0, :, :, 3] = self.GetImage(randomfolder, randomfile)
        imglist[0, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
        imglist[0, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
        imglist[0, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
        actionList = self.GetAction(randomfolder, randomfile, fullAction)
        outputlist = self.GetImage(randomfolder, randomfile + 1)
        return imglist, actionList, outputlist, randomfolder, randomfile

    def GetAction(self, episode, frame, fullAction=False):
        if self.Read_image_file == True:
            actionListfile = open(os.path.join(os.path.join(self.DataSetaddress, episode), "act.log"))
            if fullAction == True:
                line = actionListfile.readlines()
                actionListindex = [int(i) for i in line]
                del actionListindex[0:frame]
                actionList = np.zeros((len(actionListindex), self.number_of_action))
                for i in range(len(actionListindex)):
                    actionList[i, actionListindex[i]] = 1
            else:
                actionList = np.zeros((1, self.number_of_action))
                ind = int(actionListfile.readlines()[frame])
                actionList[0, ind] = 1
            actionListfile.close()
        else:
            if fullAction == True:
                actionList = np.zeros((len(self.actionlist[episode]), self.number_of_action))
                for i in range(len(self.actionlist[episode])):
                    actionList[i, self.actionlist[episode][i]] = 1
            else:
                actionList = np.zeros((1, self.number_of_action))
                actionList[0, self.actionlist[episode][frame]] = 1
        return actionList

    def GetImage(self, episode, frame):
        if self.Read_image_file == True:
            return (misc.imread(os.path.join(os.path.join(self.DataSetaddress, episode),
                                             '{:05d}'.format(frame) + ".png")) - self.average) / 255
        else:
            return (self.imagelist[episode][frame] - self.average) / 255

    def CreateMiniBatch(self, size, k=1):
        counter = 0
        imglist = np.zeros((size * k, self.input_height, self.input_width, self.historyLength))
        outputlist = np.zeros((size * k, self.input_height, self.input_width, 1))
        actionList = np.zeros((size * k, self.number_of_action))
        while counter < size:
            randomfolder, randomfile = self.GetRandomEpisodeFrame()
            actionList[counter * k, :] = self.GetAction(randomfolder, randomfile)
            imglist[counter * k, :, :, 3] = self.GetImage(randomfolder, randomfile)
            imglist[counter * k, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
            imglist[counter * k, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
            imglist[counter * k, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
            outputlist[counter * k, :, :, 0] = self.GetImage(randomfolder, randomfile + 1)
            for i in range(1, k):
                actionList[counter * k + i, :] = self.GetAction(randomfolder, randomfile + i)
                imglist[counter * k + i, :, :, 3] = np.reshape(
                    self.PredictImage([imglist[counter * k + i - 1, :, :, :]], [actionList[counter * k + i - 1, :]]),
                    (self.input_height, self.input_width))
                imglist[counter * k + i, :, :, 2] = imglist[counter * k + i - 1, :, :, 3]
                imglist[counter * k + i, :, :, 1] = imglist[counter * k + i - 1, :, :, 2]
                imglist[counter * k + i, :, :, 0] = imglist[counter * k + i - 1, :, :, 1]
                outputlist[counter * k + i, :, :, 0] = self.GetImage(randomfolder, randomfile + i + 1)
            counter = counter + 1
        return imglist, outputlist, actionList

    def PredictImage(self, img, action, outimg=None, showImg=False):
        imgout = self.sess.run(self.output, feed_dict={self.x: img, self.action: action})
        if showImg:
            imgs = np.zeros((self.input_height, 3 * self.input_width))
            imgs[:, self.input_width:2 * self.input_width] = (imgout[0, :, :, 0] + self.average / 255) * 255
            imgs[:, 0:self.input_width] = (img[0, :, :, 0] + self.average / 255) * 255
            imgs[:, 2 * self.input_width:3 * self.input_width] = (outimg + self.average / 255) * 255
            Image.fromarray(imgs).show()
        return imgout

    def ShowImage(self, _screen):
        misc.imshow(_screen)

    def PredictImages(self, img, actionList, step):
        imglist = np.zeros((self.input_height, self.input_width, step + 4))
        imglist[:, :, 0:4] = img

        for i in range(step - 4):
            imglist[:, :, 4 + i] = np.reshape(self.sess.run(self.output, feed_dict={self.x: [imglist[:, :, i:i + 4]],
                                                                                    self.action: [
                                                                                        actionList[i + 4, :]]}),
                                              newshape=(self.input_height, self.input_width))
            Image.fromarray((imglist[:, :, 4 + i] + self.average / 255) * 255).convert('RGB').save(
                "output/out" + str(i) + ".png")
        outfile = open("output/act.log", "w+")
        ac = np.argmax(actionList, axis=1).tolist()
        outfile.writelines(list("%d\n" % item for item in ac))


ff = Frame_predict()
#ff.StartTraining()
img, action, outimg, randomfolder, randomfile = ff.GetRandomImage(True)
print("folder= " + str(randomfolder) + " and file= " + str(randomfile))
ff.PredictImages(img, action, len(action))
ff.CreateOutput(randomfolder, randomfile)
