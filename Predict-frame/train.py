import bptt.dataset
import bptt.network
import tensorflow as tf
import bptt.constant
import os
from bptt.network import global_step

def StartTraining():
    sess = tf.Session()
    dataset=bptt.dataset.Dataset()

    lossPlot = tf.get_variable('loss_plot', shape=(), initializer=tf.constant_initializer(0.0),trainable=False)
    tf.summary.scalar("GeneratorLoss", lossPlot)
    networkkequalone=bptt.network.network(1,dataset,sess)
    currentNetwork=networkkequalone
    batchsize=bptt.constant.firstbatchsize

    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    merge = tf.summary.merge_all()
    writer = tf.summary.FileWriter("Log/")

    epoch=int(bptt.constant.numberofTrainningEpoch)

    if (os.path.isfile("save/model.ckpt.index")):
        saver.restore(sess, "save/model.ckpt")
    lasti=-1
    k=1
    for i in range(epoch):
        if i==bptt.constant.k1to3Switch:
            networkkequalthree=bptt.network.network(3,dataset,sess)
            currentNetwork=networkkequalthree
            batchsize=bptt.constant.secondbatchsize
            k=3
            print("K Changed to 3")
        elif i==bptt.constant.k3to5switch:
            networkkequalfive=bptt.network.network(5,dataset,sess)
            currentNetwork=networkkequalfive
            k=5
            print("K Changed to 5")
        imglist, outputlist, actionList = dataset.CreateMiniBatch(batchsize,k)
        
        _,gs=sess.run((currentNetwork.train,global_step), feed_dict={currentNetwork.x: imglist, currentNetwork.y_: outputlist, currentNetwork.action: actionList})

        if i % 3000 == 0 and i > 1:
            saver.save(sess, "save/model.ckpt")
            img, action, outimg, randomfolder, randomfile = dataset.GetRandomImage(True)
            print("folder= " + str(randomfolder) + " and file= " + str(randomfile))
            currentNetwork.PredictImageSequence(img, action, len(action))
            dataset.CreateOutput(randomfolder, randomfile)

        if int(i * 100 / epoch) > lasti:
            loss = sess.run((currentNetwork.loss), feed_dict={currentNetwork.x: imglist, currentNetwork.y_: outputlist,
                                                              currentNetwork.action: actionList})
            lasti = int(gs * 100 / epoch)
            print(str(lasti) + " Percent Completed loss is equal to " + str(loss))
            sess.run(tf.assign(lossPlot, loss))
            writer.add_summary(sess.run(merge), i)
        sess.run(tf.assign(global_step,gs+1))

StartTraining()