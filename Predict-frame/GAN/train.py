from GAN.Generator import Generator
from GAN.Discriminator import Discriminator
import tensorflow as tf
import numpy as np
import os
import constantVar


def Train():
    X = tf.placeholder(shape=(None, constantVar.input_width, constantVar.input_height, 4), dtype=tf.float32,name="xR")
    XFake=tf.placeholder(shape=(None, constantVar.input_width, constantVar.input_height, 3), dtype=tf.float32,name="xF")
    D_fake=None
    D_logit_fake=None

    if constantVar.adversarial:
        D_real, D_logit_real=disc.CreateNetwork(X,False)
        dropt,drloss=disc.CreateOptimiser(D_logit_real)

        fakeinput=tf.concat((generator.output,XFake),axis=3)
        D_fake, D_logit_fake=disc.CreateNetwork(fakeinput,True)
        dfopt,dfloss = disc.CreateOptimiser(D_logit_fake)

        tf.summary.scalar("DiscriminatorLoss", tf.reduce_sum(tf.squeeze((drloss+dfloss)/2)))

    goptimiser = generator.CreateOptimiser(D_fake, D_logit_fake)

    tf.summary.scalar("GeneratorLoss", generator.loss)

    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())
    merge = tf.summary.merge_all()
    writer = tf.summary.FileWriter("Log/")

    if (os.path.isfile("save/model.ckpt.index")):
        saver.restore(sess, "save/model.ckpt")

    for i in range(int(constantVar.numberofTrainningEpoch)):
        print(str((i / constantVar.numberofTrainningEpoch) * 100) + "Percent Completed")
        imglist, outputlist, actionList ,Predictedset= generator.CreateMiniBatch(generator.batchsize, generator.k)
        generator.UpdateGeneratorParam(i)
        batchsize=imglist.shape[0]
        sess.run(goptimiser, feed_dict={generator.x: imglist, generator.y_: outputlist, generator.action: actionList,XFake:Predictedset[:,:,:,1:]})
        if constantVar.adversarial:
            sess.run(dropt,feed_dict={X:imglist,disc.labels:np.ones(shape=(batchsize,1))})
            sess.run(dfopt, feed_dict={XFake:Predictedset[:,:,:,1:],disc.fakelabels: np.zeros(shape=(batchsize, 1)),generator.x: imglist, generator.action: actionList})
        if i % 1000 == 0 and i > 1:
            saver.save(sess, "save/model.ckpt")
            img, action, outimg, randomfolder, randomfile = generator.GetRandomImage(True)
            print("folder= " + str(randomfolder) + " and file= " + str(randomfile))
            generator.PredictImageSequence(img, action, len(action))
            generator.CreateOutput(randomfolder, randomfile)

        if i % 20 == 0:
            if constantVar.adversarial:
                mergerun,predicted = sess.run((merge,generator.output), feed_dict={generator.x: imglist, generator.y_: outputlist,XFake:Predictedset[:,:,:,1:],generator.action: actionList,X:imglist,disc.fakelabels:np.zeros(shape=(batchsize,1)),disc.labels: np.ones(shape=(batchsize,1))})
                tests = sess.run(D_real, feed_dict={X: np.concatenate((imglist[0:10], Predictedset[0:10]))})
            else:
                mergerun = sess.run((merge),feed_dict={generator.x: imglist, generator.y_: outputlist,
                                                          XFake: Predictedset[:, :, :, 1:],
                                                          generator.action: actionList, X: imglist})
            writer.add_summary(mergerun, i)



sess=tf.Session()
if constantVar.adversarial:
    disc=Discriminator(sess)
generator=Generator(sess)


Train()

