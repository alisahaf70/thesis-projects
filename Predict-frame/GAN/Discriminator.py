import tensorflow as tf
import numpy as np
from GAN.Lossfunction import bce_loss
from GAN.Generalfunc import selu
import constantVar


class Discriminator:
    def __init__(self,session):
        self.sess=session
        self.w = {}
        self.labels = tf.placeholder(shape=(None, 1), dtype=tf.float32, name="labels")
        self.fakelabels=tf.placeholder(shape=(None, 1), dtype=tf.float32, name="fakelabels")
        self.Firstinit=False
    def CreateNetwork(self,input,reused):
        with tf.variable_scope("disc",reuse=reused):
            if(reused==True):
                self.Firstinit=True
            convolutionInitializer = tf.contrib.layers.xavier_initializer_conv2d()
            LinearInitializer = tf.contrib.layers.xavier_initializer()
            BiasInitializer = tf.constant_initializer(0.0)

            self.w["D_conv1"]=tf.get_variable("D_conv1",shape=(7,7,4,128),initializer=convolutionInitializer)
            conv1=selu(tf.nn.bias_add(tf.nn.conv2d(input,self.w["D_conv1"],(1,2,2,1),"VALID",data_format="NHWC"),tf.get_variable('D_conv1biases', [128], initializer=BiasInitializer), "NHWC"))

            self.w["D_conv2"] = tf.get_variable("D_conv2", shape=(7, 7, 128, 256), initializer=convolutionInitializer)
            conv2 = selu(tf.nn.bias_add(tf.nn.conv2d(conv1, self.w["D_conv2"], (1, 2, 2, 1), "VALID", data_format="NHWC"),tf.get_variable('D_conv2biases', [256], initializer=BiasInitializer),"NHWC"))

            self.w["D_conv3"] = tf.get_variable("D_conv3", shape=(5, 5, 256,256), initializer=convolutionInitializer)
            conv3 = selu(tf.nn.bias_add(tf.nn.conv2d(conv2, self.w["D_conv3"], (1, 2, 2, 1), "VALID", data_format="NHWC"),tf.get_variable('D_conv3biases', [256], initializer=BiasInitializer),"NHWC"))

            conv3_flat = tf.reshape(conv3, shape=[-1, 7*7*256], name="D_flat")

            self.w["D_flat1"]=tf.get_variable("D_flat1",(conv3_flat.get_shape()[1],1024),initializer=LinearInitializer)
            flat1=selu(tf.nn.bias_add(tf.matmul(conv3_flat,self.w["D_flat1"]),tf.get_variable("D_flat1Bias",[1024],initializer=BiasInitializer)))

            self.w["D_flat2"] = tf.get_variable("D_flat2", (flat1.get_shape()[1], 512), initializer=LinearInitializer)
            flat2 = selu(tf.nn.bias_add(tf.matmul(flat1, self.w["D_flat2"]),
                                        tf.get_variable("D_flat2Bias", [512], initializer=BiasInitializer)))

            self.w["D_flat3"] = tf.get_variable("D_flat3", (flat2.get_shape()[1], 1), initializer=LinearInitializer)
            flat3 = tf.nn.bias_add(tf.matmul(flat2, self.w["D_flat3"]),
                                        tf.get_variable("D_flat3Bias", [1], initializer=BiasInitializer))
            return tf.nn.sigmoid(flat3),flat3

    def CreateOptimiser(self,outws):
        t_vars = tf.trainable_variables()
        d_vars = [var for var in t_vars if 'D_' in var.name]
        self.LearningRate=tf.Variable(0.0001)
        if(self.Firstinit==True):
            self.loss=bce_loss(outws,self.fakelabels)
        else:
            self.loss = bce_loss(outws, self.labels)
        optimizer = tf.train.GradientDescentOptimizer(self.LearningRate)
        self.train = optimizer.minimize(self.loss,var_list=d_vars)
        return self.train,self.loss

    def CalculateDiffrence(self,img):
        output=np.zeros((len(img),constantVar.input_width,constantVar.input_height,1))
        for j in range(len(img)):
            for i in range(constantVar.HistoryLength - 1):
                output[j,:, :, 0] = output[j,:,:,0] + (img[j][:, :, i + 1] - img[j][:, :, i])
        return output
