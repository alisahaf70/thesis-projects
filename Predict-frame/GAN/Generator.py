import tensorflow as tf
import os
import random
import numpy as np
from scipy import misc
from PIL import Image
from GAN.Lossfunction import combined_loss
from GAN.Generalfunc import selu
import pickle
import shutil
import constantVar

class Generator():
    OutputAddress = constantVar.GOutputAddress
    DataSetaddress = constantVar.GDataSetaddress
    historyLength = constantVar.HistoryLength
    input_width = constantVar.input_width
    input_height = constantVar.input_height
    Read_image_file = True
    k = 1
    number_of_action = constantVar.number_of_action
    cnn_format = "NHWC"  # NHWC
    w = {}
    batchsize = 32

    def __init__(self, session):
        print("class created")
        self.sess = session
        self.Create_Network()
        if self.Read_image_file:
            self.folderlist = os.listdir(self.DataSetaddress)

    def ResizeImage(self, img):
        return misc.imresize(img, 2.0)

    def CreateOutput(self, folder, file):
        imagesize = self.input_width * 2
        filelist = sorted(os.listdir(self.OutputAddress))
        count = 0
        for filename in filelist:
            if filename == "act.log":
                continue
            outfile = np.zeros(shape=(imagesize, imagesize * 2))
            out = self.ResizeImage(misc.imread(os.path.join(self.OutputAddress, "out" + str(count) + ".png")))
            out = Image.fromarray(out).convert("L")
            infile = self.ResizeImage(misc.imread(
                os.path.join(os.path.join(self.DataSetaddress, folder), '{:05d}'.format(file + count+self.historyLength+1) + ".png")))
            outfile[:, :imagesize] = out
            outfile[:, imagesize:] = infile
            Image.fromarray(outfile).convert('RGB').save("output/zout" + str(count) + ".png")
            os.remove(os.path.join(self.OutputAddress, "out" + str(count) + ".png"))
            count += 1

    def CreateOptimiser(self,dans,danslogit):
        t_vars = tf.trainable_variables()

        g_vars = [var for var in t_vars if 'G_' in var.name]
        self.LearningRate = tf.Variable(1e-4,trainable=False)
        self.loss = combined_loss(self.output, self.y_,danslogit)
        optimizer = tf.train.AdamOptimizer(self.LearningRate, 0.9, 0.95)
        self.train = optimizer.minimize(self.loss,var_list=g_vars)
        return self.train

    def Create_Network(self):
        initializer = tf.contrib.layers.xavier_initializer_conv2d()
        LinearInitializer = tf.contrib.layers.xavier_initializer()
        BiasInitializer = tf.constant_initializer(0.0)
        if self.Read_image_file == False:
            with open('images', 'rb') as fp:
                self.imagelist = pickle.load(fp, encoding='latin1')
            with open('actions', 'rb') as fp:
                self.actionlist = pickle.load(fp, encoding='latin1')
        self.average = np.load("mean.npy")
        # self.ShowImage(self.average)
        self.action = tf.placeholder(dtype=tf.float32, shape=(None, self.number_of_action),name="Action")
        self.x = tf.placeholder(dtype=tf.float32, shape=(None, self.input_height, self.input_width, self.historyLength),name="Generatorinput")
        self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, self.input_height, self.input_width, 1),name="generatorlabel")
        self.w["G_Wconv1"] = tf.get_variable("G_Wconv1", shape=[6, 6, self.historyLength, 64], initializer=initializer)
        conv1 = tf.nn.conv2d(self.x, filter=self.w["G_Wconv1"], strides=[1, 2, 2, 1], data_format=self.cnn_format,
                             padding="VALID", name="G_conv1")
        relu_conv1 = selu(
            tf.nn.bias_add(conv1, tf.get_variable('G_conv1biases', [64], initializer=BiasInitializer), self.cnn_format))

        self.w["G_Wconv2"] = tf.get_variable("G_Wconv2", shape=[6, 6, relu_conv1.get_shape()[-1], 64],
                                           initializer=initializer)
        conv2 = tf.nn.conv2d(relu_conv1, self.w["G_Wconv2"], strides=[1, 2, 2, 1], data_format=self.cnn_format,
                             padding="SAME", name="G_conv2")
        relu_conv2 = selu(
            tf.nn.bias_add(conv2, tf.get_variable('G_conv2biases', [64], initializer=BiasInitializer), self.cnn_format))

        self.w["G_Wconv3"] = tf.get_variable("G_Wconv3", shape=[6, 6, relu_conv2.get_shape()[-1], 64],
                                           initializer=initializer)
        conv3 = tf.nn.conv2d(relu_conv2, self.w["G_Wconv3"], strides=[1, 2, 2, 1], data_format=self.cnn_format,
                             padding="SAME", name="G_conv3")
        relu_conv3 = selu(
            tf.nn.bias_add(conv3, tf.get_variable('G_conv3biases', [64], initializer=BiasInitializer), self.cnn_format))
        conv3_flat = tf.reshape(relu_conv3, shape=[-1, 10 * 10 * 64], name="G_l3flat")

        self.w["G_Wflat1"] = tf.get_variable("G_WFlat1", shape=[10 * 10 * 64, 1024], initializer=LinearInitializer)
        l1flat = selu(tf.nn.bias_add(tf.matmul(conv3_flat, self.w["G_Wflat1"]),
                                           tf.get_variable('G_flat1biases', [1024], initializer=BiasInitializer)))

        self.w["G_Wflat2"] = tf.get_variable("G_WFlat2", shape=[1024, 2048], initializer=LinearInitializer)
        l2flat = tf.nn.bias_add(tf.matmul(l1flat, self.w["G_Wflat2"]),
                                tf.get_variable('G_flat2biases', [2048], initializer=BiasInitializer))

        self.w["G_Wflattemp"] = tf.get_variable("G_Wflattemp", shape=[2048, 4096],
                                              initializer=tf.random_uniform_initializer(-1, 1))
        ltempflat = tf.nn.bias_add(tf.matmul(l2flat, self.w["G_Wflattemp"]),
                                   tf.get_variable('G_flattempbiases', [4096], initializer=BiasInitializer))

        self.w["G_WActionflat"] = tf.get_variable("G_WActionflat", shape=[self.number_of_action, 4096],
                                                initializer=tf.random_uniform_initializer(-0.1, 0.1))
        actionflat = tf.nn.bias_add(tf.matmul(self.action, self.w["G_WActionflat"]),
                                    tf.get_variable('G_actionflatbias', [4096], initializer=BiasInitializer))

        transformation = tf.multiply(actionflat, ltempflat, "G_Combine_Layer")

        self.w["G_Wflat3"] = tf.get_variable("G_Wflat3", shape=[4096, 2048], initializer=LinearInitializer)
        l3flat = tf.nn.bias_add(tf.matmul(transformation, self.w["G_Wflat3"]),
                                tf.get_variable('G_flat3biases', [2048], initializer=BiasInitializer))

        self.w["G_Wflat4"] = tf.get_variable("G_Wflat4", shape=[2048, 1024], initializer=LinearInitializer)
        l4flat = tf.nn.bias_add(selu(tf.matmul(l3flat, self.w["G_Wflat4"])),
                                tf.get_variable('G_flat4biases', [1024], initializer=BiasInitializer))

        self.w["G_Wflat5"] = tf.get_variable("G_Wflat5", shape=[1024, 64 * 10 * 10], initializer=LinearInitializer)
        l5flat = tf.nn.bias_add(selu(tf.matmul(l4flat, self.w["G_Wflat5"])),
                                tf.get_variable('G_flat5biases', [64 * 10 * 10], initializer=BiasInitializer))

        l5 = tf.reshape(l5flat, (-1, 10, 10, 64),name="G_l6flat")

        self.w["G_WDeconv1"] = tf.get_variable("G_WDeconv1", shape=(6, 6, 64, 64), initializer=initializer)
        deconv1 = selu(tf.nn.bias_add(tf.nn.conv2d_transpose(l5, self.w["G_WDeconv1"], strides=[1, 2, 2, 1],
                                                                   output_shape=(tf.shape(self.x)[0], 20, 20, 64),
                                                                   padding="SAME", data_format=self.cnn_format,
                                                                   name="G_deconv1"),
                                            tf.get_variable('G_deconv1biases', [64], initializer=BiasInitializer)))

        self.w["G_WDeconv2"] = tf.get_variable("G_WDeconv2", shape=(6, 6, 64, 64), initializer=initializer)
        deconv2 = selu(tf.nn.bias_add(tf.nn.conv2d_transpose(deconv1, self.w["G_WDeconv2"], strides=[1, 2, 2, 1],
                                                                   output_shape=(tf.shape(self.x)[0], 40, 40, 64),
                                                                   padding="SAME", data_format=self.cnn_format,
                                                                   name="G_deconv2"),
                                            tf.get_variable('G_deconv2biases', [64], initializer=BiasInitializer)))

        self.w["G_WDeconv3"] = tf.get_variable("G_WDeconv3", shape=(6, 6, 1, 64), initializer=initializer)
        self.deconv3 = tf.nn.bias_add(tf.nn.conv2d_transpose(deconv2, self.w["G_WDeconv3"], strides=[1, 2, 2, 1],
                                                             output_shape=(
                                                             tf.shape(self.x)[0], self.input_height, self.input_width,
                                                             1), padding="VALID", data_format=self.cnn_format,
                                                             name="G_deconv3"),
                                      tf.get_variable('G_deconv3biases', [1], initializer=BiasInitializer))
        self.output = tf.nn.tanh(self.deconv3)
        return self.output

    def UpdateGeneratorParam(self,counter):
        if counter == 5000 and self.batchsize!=8:
            self.batchsize = 8
            self.k = 3
            self.sess.run(tf.assign(self.LearningRate, 1e-5))
        elif counter == 10000:
            self.k = 5
    def GetRandomEpisodeFrame(self):
        while True:
            if self.Read_image_file == True:
                episode = random.choice(self.folderlist)
                filelist = os.listdir(os.path.join(self.DataSetaddress, episode))
                if (len(filelist) < 3 + self.k + 6):
                    continue
                frame = random.randrange(3, len(filelist) - 5- self.k)
            else:
                episode = random.randrange(0, len(self.imagelist))
                if (len(self.imagelist[episode]) < 3 + self.k + 2):
                    continue
                frame = random.randrange(3, len(self.imagelist[episode]) - 1 - self.k)
            return episode, frame


    def GetRandomImage(self, fullAction=False):
        randomfolder, randomfile = self.GetRandomEpisodeFrame()

        imglist = np.zeros((1, self.input_height, self.input_width, self.historyLength))
        imglist[0, :, :, 3] = self.GetImage(randomfolder, randomfile)
        imglist[0, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
        imglist[0, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
        imglist[0, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
        actionList = self.GetAction(randomfolder, randomfile, fullAction)
        outputlist = self.GetImage(randomfolder, randomfile + 1)
        return imglist, actionList, outputlist, randomfolder, randomfile


    def GetAction(self, episode, frame, fullAction=False):
        if self.Read_image_file == True:
            actionListfile = open(os.path.join(os.path.join(self.DataSetaddress, episode), "act.log"))
            if fullAction == True:
                line = actionListfile.readlines()
                actionListindex = [int(i) for i in line]
                del actionListindex[0:frame]
                actionList = np.zeros((len(actionListindex), self.number_of_action))
                for i in range(len(actionListindex)):
                    actionList[i, actionListindex[i]] = 1
            else:
                actionList = np.zeros((1, self.number_of_action))
                ind = int(actionListfile.readlines()[frame])
                actionList[0, ind] = 1
            actionListfile.close()
        else:
            if fullAction == True:
                actionList = np.zeros((len(self.actionlist[episode]), self.number_of_action))
                for i in range(len(self.actionlist[episode])):
                    actionList[i, self.actionlist[episode][i]] = 1
            else:
                actionList = np.zeros((1, self.number_of_action))
                actionList[0, self.actionlist[episode][frame]] = 1
        return actionList


    def GetImage(self, episode, frame):
        if self.Read_image_file == True:
            return (misc.imread(os.path.join(os.path.join(self.DataSetaddress, episode),
                                             '{:05d}'.format(frame) + ".png")) - self.average) / 255
        else:
            return (self.imagelist[episode][frame] - self.average) / 255


    def CreateMiniBatch(self, size, k=1):
        counter = 0
        imglist = np.zeros((size * k, self.input_height, self.input_width, self.historyLength))
        outputlist = np.zeros((size * k, self.input_height, self.input_width, 1))
        actionList = np.zeros((size * k, self.number_of_action))
        fullactionList = np.zeros((size * k,constantVar.HistoryLength, self.number_of_action))
        while counter < size:
            randomfolder, randomfile = self.GetRandomEpisodeFrame()
            actionList[counter * k, :] = self.GetAction(randomfolder, randomfile)
            fullactionList[counter*k,:,:]=self.GetAction(randomfolder,randomfile,True)[0:constantVar.HistoryLength,:]
            imglist[counter * k, :, :, 3] = self.GetImage(randomfolder, randomfile)
            imglist[counter * k, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
            imglist[counter * k, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
            imglist[counter * k, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
            outputlist[counter * k, :, :, 0] = self.GetImage(randomfolder, randomfile + 1)#-self.GetImage(randomfolder, randomfile)
            for i in range(1, k):
                actionList[counter * k + i, :] = self.GetAction(randomfolder, randomfile + i)
                imglist[counter * k + i, :, :, 2] = imglist[counter * k + i - 1, :, :, 3]
                imglist[counter * k + i, :, :, 1] = imglist[counter * k + i - 1, :, :, 2]
                imglist[counter * k + i, :, :, 0] = imglist[counter * k + i - 1, :, :, 1]
                imglist[counter * k + i, :, :, 3] = np.reshape(
                    self.PredictImage([imglist[counter * k + i - 1, :, :, :]], [actionList[counter * k + i - 1, :]]),
                    (self.input_height, self.input_width))
                outputlist[counter * k + i, :, :, 0] = self.GetImage(randomfolder, randomfile + i + 1)#-self.GetImage(randomfolder, randomfile+i)
            counter = counter + 1
        return imglist, outputlist, actionList,self.ConvertToFullPredict(imglist,fullactionList)

    def ConvertToFullPredict(self,img,action):
        copyimg=np.copy(img)
        for j in range(copyimg.shape[3]):
            tempimg=self.sess.run(self.output,feed_dict={self.x:img,self.action:np.reshape(action[:,j,:],newshape=(img.shape[0],constantVar.number_of_action))})
            copyimg[:,:,:,0]=copyimg[:,:,:,1]
            copyimg[:, :, :, 1]=copyimg[:,:,:,2]
            copyimg[:, :, :, 2]=copyimg[:,:,:,3]
            copyimg[:, :, :, 3]=np.reshape(tempimg,newshape=(tempimg.shape[0],constantVar.input_width,constantVar.input_height))
        return copyimg




    def PredictImage(self, img, action, outimg=None, showImg=False):
        imgout = self.sess.run(self.output, feed_dict={self.x: img, self.action: action})
        if showImg:
            imgs = np.zeros((self.input_height, 3 * self.input_width))
            imgs[:, self.input_width:2 * self.input_width] = (imgout[0, :, :, 0] + self.average / 255) * 255
            imgs[:, 0:self.input_width] = (img[0, :, :, 0] + self.average / 255) * 255
            imgs[:, 2 * self.input_width:3 * self.input_width] = (outimg + self.average / 255) * 255
            Image.fromarray(imgs).show()
        return imgout


    def ShowImage(self, _screen):
        misc.imshow(_screen)


    def PredictImageSequence(self, img, actionList, step):
        shutil.rmtree('output')
        os.mkdir('output')
        imglist = np.zeros((self.input_height, self.input_width, step + self.historyLength))
        imglist[:, :, 0:self.historyLength] = img

        for i in range(step - self.historyLength):
            imglist[:, :, self.historyLength + i] = np.reshape(self.sess.run(self.output, feed_dict={self.x: [imglist[:, :, i:i + self.historyLength]],
                                                                                    self.action: [actionList[i + self.historyLength, :]]}),
                                              newshape=(self.input_height, self.input_width))
            Image.fromarray((imglist[:, :, self.historyLength + i] + self.average / 255) * 255).convert('RGB').save(
                "output/out" + str(i) + ".png")
        outfile = open("output/act.log", "w+")
        ac = np.argmax(actionList, axis=1).tolist()
        outfile.writelines(list("%d\n" % item for item in ac))
