import sys
import os

from ModelBased.agent import DQNAgent, Agent

sys.path.insert(0,os.getcwd())


import RewardPrediction.run as RewardPredictor
import FramePrediction.run as FramePredictor
from futuristicAgent.futuristicAgent import FuturisticAgent
from futuristicAgent.futuristicAgentDQN import FuturisticAgentDQN
from ModelBased.agent import *
import numpy as np
from ModelBased.env.atari_wrappers_deprecated import wrap_dqn
from ModelBased.env.misc_util import SimpleMonitor
import gym
import RewardPrediction.constant as rewconst
import random

def FutureAgent():
    agent=DQNAgent()
    fp=FramePredictor.FramePrediction(agent=agent)
    rp=RewardPredictor.RewardPrediction()
    env = gym.make("Freeway" + "NoFrameskip-v4")
    env = SimpleMonitor(env)
    env = wrap_dqn(env)
    obs = env.reset()
    count=0
    while True:
        obs = np.asarray([obs._frames]).squeeze(axis=4)
        obs = np.transpose(obs, [0, 2, 3, 1])
        outseq = fp.CreateRepresentationPredictionforAction(obs, 20)
        outrewtemp = rp.GetPrediction(outseq)
        [outrew] = np.reshape(outrewtemp, newshape=(np.array(obs).shape[0], env.action_space.n, outrewtemp.shape[1]))
        act, _, _ = agent.GetBestAction(obs)
        if rewconst.Classification:
            best = []
            worse = []
            zero = []
            if rewconst.ThreeClasses:
                for i in range(outrew.shape[0]):
                    if np.argmax(outrew[i])==2:
                        best.append(i)
                    elif np.argmax(outrew[i])==0:
                        worse.append(i)
                    else:
                        zero.append(i)
                if True and len(best)>0:
                    act=np.random.choice(best)
                    print("actition given from Best")
                else:
                    if act in worse and len(best)>0:
                        act=np.random.choice(best)
                        print("action fixed by best")
                    elif act in worse and len(zero)>0:
                        act=np.random.choice(zero)
                        print("action fixed by zero")
            else:
                for i in range(outrew.shape[0]):
                    if np.argmax(outrew[i])==2:
                        best.append(i)
                    elif np.argmax(outrew[i])==0:
                        worse.append(i)
                    else:
                        zero.append(i)
        else:
            a=np.argmax(outrew,axis=0)
            if  a!=act and outrew[a]!=outrew[act]:
                act=a
                count+=1
                print ("false positive"+str(count))

        obs, rew, done, info = env.step(act)
        env.render()
        if done:
            obs = env.reset()
    return env

def agen():
    agent=Agent()
    env = gym.make("Breakout" + "NoFrameskip-v4")
    env = SimpleMonitor(env)
    env = wrap_dqn(env)
    obs = env.reset()
    while True:
        obs = np.asarray([obs._frames]).squeeze(axis=4)
        obs = np.transpose(obs, [0, 2, 3, 1])
        act,_,_=agent.GetBestAction(obs)
        obs, rew, done, info = env.step(act)
        env.render()
        if done:
            obs = env.reset()
    return env

def RewardCheck():
    agent=DQNAgent()
    fp=FramePredictor.FramePrediction(agent=agent)
    re=RewardPredictor.RewardPrediction()
    while True:
        a,b=fp.CreateTrainingsetforReward(100,rewconst.Lstm_cell)
        c=re.GetPrediction(a)
        counter=0
        if rewconst.Classification:
            for i in range(c.shape[0]):
                if rewconst.ThreeClasses or rewconst.isSequence:
                    if np.argmax(c[i])!=np.argmax(b[i]):
                        counter+=1
                else:
                    if np.round(c[i])!=b[i]:
                        counter += 1
        cc=counter/a.shape[0]
        print(cc)

def CreateoutputforPrediction():
    agent = DQNAgent()
    fp = FramePredictor.FramePrediction(agent=agent)
    fp.CreateOutput()

def TrainfuturesticAgent():
    agent = DQNAgent()
    fp = FramePredictor.FramePrediction(agent=agent)
    rp=RewardPredictor.RewardPrediction()
    futuresticAgent=FuturisticAgentDQN(agent,fp,rp)
    futuresticAgent.Train()
def labelrepresentation():
    rp=RewardPredictor.RewardPrediction()
    rp.GetPerFramePrediction()



def FutureAgent2():
    agent=DQNAgent()
    fp=FramePredictor.FramePrediction(agent=agent)
    rp=RewardPredictor.RewardPrediction()
    env = gym.make("Freeway" + "NoFrameskip-v4")
    env = SimpleMonitor(env)
    env = wrap_dqn(env)
    obs = env.reset()
    count=0
    coldstart=0
    step=coldstart
    np.zeros((20,))
    lastact=0
    while True:
        obs = np.asarray([obs._frames]).squeeze(axis=4)
        obs = np.transpose(obs, [0, 2, 3, 1])

        act, _, _ = agent.GetBestAction(obs)
        if step==0:
            time = np.zeros((3))
            time[2]=1000
            obs = fp.converttoPrediction((np.copy(np.asarray(obs, dtype=np.float32))))
            for i in [0, 1]:
                time[i] = rp.GetRewardTimeForAction(obs, i, fp,lastact)
            if(np.min(time)<19):
                act=np.argmin(time)
                if time[0]==time[1] and act==0:
                    act=1
                print ("action "+str(time)+" selected")
        else:
            step-=1
        lastact=act
        obs, rew, done, info = env.step(act)
        if rew==1:
            step=coldstart
        env.render()
        if done:
            step=coldstart
            obs = env.reset()

def myagent():
    agent = DQNAgent()
    fp = FramePredictor.FramePrediction(agent=agent)
    rp = RewardPredictor.RewardPrediction()
    futuresticAgent = FuturisticAgentDQN(agent, fp, rp)
    env = gym.make("Freeway" + "NoFrameskip-v4")
    env = SimpleMonitor(env)
    env = wrap_dqn(env)
    obs = env.reset()
    totalrew=0
    while True:
        obs = np.asarray([obs._frames]).squeeze(axis=4)
        obs = np.transpose(obs, [0, 2, 3, 1])
        act=futuresticAgent.GetBestAction(obs)
        obs, rew, done, info = env.step(act)
        totalrew+=rew
        #env.render()
        if done:
            obs = env.reset()
            print (totalrew)
            totalrew=0
    return env


myagent()