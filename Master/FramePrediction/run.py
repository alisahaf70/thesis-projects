import sys
import os
sys.path.insert(0,os.getcwd()+"/FramePrediction/")
import network
import tensorflow as tf
import constant
from dataset import Dataset
import numpy as np
from PIL import Image
class FramePrediction:
    def __init__(self,agent=None):
        self.agent=agent
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.sess = tf.Session()
            self.dataset=Dataset()

            self.predictor=network.network(self.dataset,self.sess,False)

            self.sess.run(tf.global_variables_initializer())

            if (os.path.isfile(constant.SaveFileAddress+"model.ckpt.index")):
                saver = tf.train.Saver()
                saver.restore(self.sess, constant.SaveFileAddress+"model.ckpt")
            else:
                print("Can not Load Pretrain Network")
                exit(0)
    def CreateOutput(self):
        img, action, outimg, randomfolder, randomfile = self.dataset.GetRandomImage(True)
        print("folder= " + str(randomfolder) + " and file= " + str(randomfile))
        self.predictor.PredictImageSequence(img, action, len(action))
        self.dataset.CreateOutput(randomfolder, randomfile)

    def CreateTrainingsetforReward(self,batchsize,cell_size):
        outputseq, sumrewarr = self.dataset.CreateMiniBatchWithFuture(batchsize,cell_size,self.predictor)
        return outputseq, sumrewarr
    def converttoPrediction(self,image):
        for i in range(image.shape[0]):
            for j in range(image.shape[3]):
                image[i,:,:,j]=(image[i,:,:,j]-self.dataset.GetMean())/255
        return image
    def converttoRealfromPrediction(self,image):
        for i in range(image.shape[0]):
            for j in range(image.shape[3]):
                image[i,:,:,j]=image[i,:,:,j]*255+self.dataset.GetMean()
        return image
    def PredictRepresentation(self,image,action,length):
        image=self.converttoPrediction(np.copy([image]))
        repList = np.zeros((image.shape[0], length, 1024))
        for j in range(image.shape[0]):
            for i in range(length):
                actiononehot=np.zeros(shape=(1,constant.number_of_action))
                actiononehot[0,action]=1
                img,rep=self.sess.run((self.predictor.out,self.predictor.rep),feed_dict={self.predictor.x:[image[j,:,:,:]],self.predictor.action:actiononehot})
                image[j,:,:,0]=image[j,:,:,1]
                image[j,:,:,1]=image[j,:,:,2]
                image[j,:,:,2]=image[j,:,:,3]
                image[j,:,:,3]=img[:,:,:,0]
                action,_,_=self.agent.GetBestAction(self.converttoRealfromPrediction(np.copy([image[j,:,:,:]])))
                repList[j,i,:]=rep
        #self.dataset.ShowImage(np.reshape(img[0, :, :, 0] * 255 + self.dataset.GetMean(), newshape=(84, 84))).show()
        return repList
    def GetRepresentation(self,image,actiononehot):
        img, rep = self.sess.run((self.predictor.out, self.predictor.rep),
                                 feed_dict={self.predictor.x: image[:, :, :, :], self.predictor.action: actiononehot})
        return rep
    def PredictRepresentationfortest(self,image,action,length):
        image=self.converttoPrediction(np.copy(image))
        __batchsize=image.shape[0]
        repList = np.zeros((__batchsize, length, 1024))
        counter=0
        a = 0
        if a:
            ll = (image[0, :, :, 3].reshape(84, 84) + self.dataset.GetMean() / 255) * 255
            Image.fromarray(ll).convert('RGB').save(constant.OutputAddress + "out.png")
        for i in range(length):
            actiononehot = self.convertToOneHot(action)
            img, rep = self.sess.run((self.predictor.out, self.predictor.rep),feed_dict={self.predictor.x: image[:, :, :, :], self.predictor.action: actiononehot})
            image[:, :, :, 0] = image[:, :, :, 1]
            image[:, :, :, 1] = image[:, :, :, 2]
            image[:, :, :, 2] = image[:, :, :, 3]
            image[:, :, :, 3] = img[:, :, :, 0]

            if a:
                for i in range(img.shape[0]):
                    ll=(img[i, :, :, 0].reshape(84,84) + self.dataset.GetMean() / 255) * 255
                    Image.fromarray(ll).convert('RGB').save(constant.OutputAddress + "out" + str(i)+ "_"+str(counter)+ ".png")
            counter+=1
            action,_,_ = self.agent.GetBestAction(self.converttoRealfromPrediction(np.copy(image[:, :, :, :])))
            repList[:, i, :] = rep
        #self.dataset.ShowImage(np.reshape(img[0, :, :, 0] * 255 + self.dataset.GetMean(), newshape=(84, 84))).show()
        return repList

    def convertToOneHot(self,invec):
        result = np.zeros(shape=(len(invec), constant.number_of_action))
        result[np.arange(len(invec)), invec] = 1
        return result.astype(int)
    def CreateRepresentationPredictionforAction(self,image,length):
        image=np.copy(image)
        actionlist = np.ones(shape=image.shape[0]*constant.number_of_action)
        imagelist=np.zeros(shape=(image.shape[0]*constant.number_of_action,image.shape[1],image.shape[2],image.shape[3]))
        for i in range(constant.number_of_action):
            actionlist[i*image.shape[0]:(i+1)*image.shape[0]]=np.ones(shape=image.shape[0])*i
            imagelist[i*image.shape[0]:(i+1)*image.shape[0],:,:,:]=np.copy(image)
        outputseq=self.PredictRepresentationfortest(imagelist,actionlist.astype(int) , length)

        #outputseq=np.reshape(outputseq,newshape=(image.shape[0],constant.number_of_action,outputseq.shape[1],outputseq.shape[2]))
        return outputseq

    def GetPerFramePrediction(self,image,length,action):
        repList = np.zeros((length, 1024))
        for i in range(length):
            actiononehot = np.zeros(shape=(1, constant.number_of_action))
            img, rep = self.sess.run((self.predictor.out, self.predictor.rep),
                                         feed_dict={self.predictor.x: [image[0, :, :, :]],
                                                    self.predictor.action: [action[i]]})
            image[0, :, :, 0] = image[0, :, :, 1]
            image[0, :, :, 1] = image[0, :, :, 2]
            image[0, :, :, 2] = image[0, :, :, 3]
            image[0, :, :, 3] = img[:, :, :, 0]
            repList[i, :] = rep
        return repList

    def GetPerFramePredictionforingame(self,image,length,action):#farghesh ba balayi ine ke actionesh yekie
        repList = np.zeros((length, 1024))
        action=[action]
        for i in range(length):
            actiononehot = self.convertToOneHot(action)
            img, rep = self.sess.run((self.predictor.out, self.predictor.rep),
                                         feed_dict={self.predictor.x: [image[0, :, :, :]],
                                                    self.predictor.action: actiononehot})
            image[0, :, :, 0] = image[0, :, :, 1]
            image[0, :, :, 1] = image[0, :, :, 2]
            image[0, :, :, 2] = image[0, :, :, 3]
            image[0, :, :, 3] = img[:, :, :, 0]
            action, _, _ = self.agent.GetBestAction(self.converttoRealfromPrediction(np.copy(image[:, :, :, :])))
            repList[i, :] = rep
        return repList