import os
import argparse
import numpy as np
import tensorflow as tf

MODEL_DIR = os.path.join("/home/mll/@li/thesis-projects/Master/FramePrediction/ExpertModel/games/Freeway/", 'model')


class Agent:
    def __init__(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        g = tf.Graph()
        with g.as_default():
            self.sess = tf.Session(config=config)
            saver = tf.train.import_meta_graph(os.path.join(MODEL_DIR, 'model.meta'))
            saver.restore(self.sess, tf.train.latest_checkpoint(MODEL_DIR))
            self.input = g.get_tensor_by_name('global/Placeholder:0')
            self.pi = g.get_tensor_by_name('global/fully_connected_2/Softmax:0')
            self.output = tf.argmax(self.pi, axis=1)

    def GetBestAction(self, x):
        [a] = self.sess.run(self.output, feed_dict={self.input: x})
        return a
a=Agent()