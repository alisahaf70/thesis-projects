import numpy as np
import random
import os
from scipy import misc
from PIL import Image
import RewardPrediction.constant as rewconst
import math
from FramePrediction import constant
from PIL import ImageChops
class Dataset:
    mean=None # Mean of DatasetImages
    def __init__(self):
        if constant.OneEpisode==False:
            self.folderlist = os.listdir(constant.DatasetAddress)
        else:
            self.actionLine = open(os.path.join(constant.RewardAddress, "act.log")).readlines()
            self.rewardLine = np.asarray(open(os.path.join(constant.RewardAddress, "rew.log")).readlines())
            self.rewardLine[self.rewardLine== ''] = 0.0
            self.rewardLine = (self.rewardLine.astype(np.float)).astype(int)
            [self.rewpositive]= np.where(self.rewardLine==1)
            [self.rewnegetive] = np.where(self.rewardLine == -1)
            [self.rewzero]=np.where(self.rewardLine==0)
        self.mean = np.load(constant.MeanFileAddress)
        return
    def GetMean(self):
        #self.ShowImage(self.mean)
        return self.mean

    def CreateMiniBatch(self, generator,size, k=1):
        counter = 0
        imglist = np.zeros((size * k, constant.input_height, constant.input_width, constant.historyLength))
        outputlist = np.zeros((size * k, constant.input_height, constant.input_width, 1))
        actionList = np.zeros((size * k, constant.number_of_action))
        while counter < size:
            randomfolder, randomfile,_ = self.GetRandomEpisodeFrame()
            actionList[counter * k, :] = self.GetAction(randomfolder, randomfile)
            imglist[counter * k, :, :, 3] = self.GetImage(randomfolder, randomfile)
            imglist[counter * k, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
            imglist[counter * k, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
            imglist[counter * k, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
            outputlist[counter * k, :, :, 0] = self.GetImage(randomfolder,randomfile + 1)
            for i in range(1, k):
                actionList[counter * k + i, :] = self.GetAction(randomfolder, randomfile + i)
                imglist[counter * k + i, :, :, 2] = imglist[counter * k + i - 1, :, :, 3]
                imglist[counter * k + i, :, :, 1] = imglist[counter * k + i - 1, :, :, 2]
                imglist[counter * k + i, :, :, 0] = imglist[counter * k + i - 1, :, :, 1]
                imglist[counter * k + i, :, :, 3] = np.reshape(
                    generator.PredictImage([imglist[counter * k + i - 1, :, :, :]], [actionList[counter * k + i - 1, :]]),
                    (constant.input_height, constant.input_width))
                outputlist[counter * k + i, :, :, 0] = self.GetImage(randomfolder,randomfile + i + 1)
            counter = counter + 1
        return imglist, outputlist, actionList
    def ShowImage(self, _screen):
        misc.imshow(_screen)
    def GetRandomEpisodeFrame(self):
        if constant.OneEpisode:
            frame = random.randrange(3, len(self.actionLine) - 21)
            episode=None
            length=len(self.actionLine)
        else:
            while True:
                episode = random.choice(self.folderlist)
                filelist = os.listdir(os.path.join(constant.DatasetAddress, episode))
                filelist.remove("act.log")
                filelist.remove("rew.log")
                if (len(filelist) < constant.historyLength+rewconst.Lstm_cell+5):
                    continue
                frame = random.randrange(3, len(filelist) - rewconst.Lstm_cell-2*constant.historyLength)
                length=len(filelist)
                return episode, frame,length
        return episode, frame, length

    def GetAction(self, episode, frame, fullAction=False):
        if constant.OneEpisode==False:
            actionListfile = open(os.path.join(os.path.join(constant.DatasetAddress, episode), "act.log"))
            line = actionListfile.readlines()
            actionListfile.close()
        else:
            line=self.actionLine
        if fullAction == True:
            actionListindex = [int(i) for i in line]
            del actionListindex[0:frame]
            actionList = np.zeros((len(actionListindex), constant.number_of_action))
            for i in range(len(actionListindex)):
                actionList[i, actionListindex[i]] = 1
        else:
            actionList = np.zeros((1, constant.number_of_action))
            ind = int(line[frame])
            actionList[0, ind] = 1
        return actionList

    def GetReward(self, episode, frame):
        if constant.OneEpisode:
            ind = self.rewardLine[frame]
        else:
            actionListfile = open(os.path.join(os.path.join(constant.DatasetAddress, episode), "rew.log"))
            ind = int(float(actionListfile.readlines()[frame]))
            actionListfile.close()
        return ind

    def GetImage(self, episode, frame):
        if constant.OneEpisode:
            return (misc.imread(os.path.join(constant.DatasetAddress,
                                                 '{:06d}'.format(frame) + ".png")) - self.GetMean()) / 255
        else:
            return (misc.imread(os.path.join(os.path.join(constant.DatasetAddress, episode),
                                             '{:05d}'.format(frame) + ".png")) - self.GetMean()) / 255

    def GetRandomImage(self, fullAction=False,withrew=False):
        randomfolder, randomfile,_ = self.GetRandomEpisodeFrame()
        if withrew:
            aa=self.GetRewardFrame(randomfolder,1)[0]
            if aa>15:#just for frame per len prediction
                randomfile=np.random.randint(aa-15,aa-5,1)[0]-20
                print('reward is in '+str(aa-randomfile-20)+' sequence')
        imglist = np.zeros((1, constant.input_height, constant.input_width, constant.historyLength))
        imglist[0, :, :, 3] = self.GetImage(randomfolder, randomfile)
        imglist[0, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
        imglist[0, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
        imglist[0, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
        actionList = self.GetAction(randomfolder, randomfile, fullAction)
        outputlist = self.GetImage(randomfolder, randomfile + 1)
        return imglist, actionList, outputlist, randomfolder, randomfile

    def ResizeImage(self, img):
        return misc.imresize(img, 2.0)

    def CreateOutput(self, folder, file):
        imagesize = constant.input_width * 2
        filelist = sorted(os.listdir(constant.OutputAddress))
        count = 0
        for filename in filelist:
            if filename == "act.log":
                continue
            outfile = np.zeros(shape=(imagesize, imagesize * 3))
            out = self.ResizeImage(misc.imread(os.path.join(constant.OutputAddress, "out" + str(count) + ".png")))
            out = Image.fromarray(out).convert("L")
            if constant.OneEpisode:
                infile = self.ResizeImage(misc.imread(
                    os.path.join(constant.DatasetAddress,
                                 '{:06d}'.format(file + count  + 1) + ".png")))
            else:
                infile = self.ResizeImage(misc.imread(
                    os.path.join(os.path.join(constant.DatasetAddress, folder), '{:05d}'.format(file + count+1) + ".png")))
            outfile[:, :imagesize] = infile
            outfile[:, imagesize:2*imagesize] = out
            outfile[:, 2 * imagesize:] = ImageChops.difference(out, Image.fromarray(infile))
            Image.fromarray(outfile).convert('RGB').save(constant.OutputAddress+"zout" + str(count) + ".png")
            os.remove(os.path.join(constant.OutputAddress, "out" + str(count) + ".png"))
            count += 1
    def CreateMiniBatchWithFuture(self, size, k,generator):
        counter = 0
        imglist = np.zeros((size * k, constant.input_height, constant.input_width,constant.historyLength))
        outputseq=np.zeros(shape=(size,k,1024))
        if rewconst.Classification==False:
            sumrewarr=np.zeros(shape=(size, 1))
        elif rewconst.isSequence:
            sumrewarr = np.zeros(shape=(size, k))
        elif rewconst.ThreeClasses:
            sumrewarr=np.zeros(shape=(size,3))
        elif rewconst.outputTime:
            sumrewarr=np.zeros(shape=(size,1))
        else:
            sumrewarr = np.zeros(shape=(size, 1))
        actionList = np.zeros(shape=(size * k, constant.number_of_action))
        numberofRewardedBatch=0
        numberofPunishedBatch = 0
        divide=2
        if rewconst.ThreeClasses:
            divide=3
        while counter < size:
            randomfolder, randomfile,lenfile = self.GetRandomEpisodeFrame()
            if(numberofRewardedBatch<(size/divide)-1 or rewconst.outputTime):
                lst=self.GetRewardFrame(randomfolder,1)
                if len(lst)>0:
                    randomfile2=random.randrange(0,len(lst))
                    randomfile2=random.randrange(lst[randomfile2]-k+1,lst[randomfile2])
                    if randomfile2>4 and randomfile2<lenfile-(k+1):
                        randomfile=randomfile2
            elif(rewconst.ThreeClasses and numberofPunishedBatch<(size/divide)-1):
                lst = self.GetRewardFrame(randomfolder, -1)
                if len(lst) > 0:
                    randomfile2 = random.randrange(0, len(lst))
                    randomfile2 = random.randrange(lst[randomfile2] - k + 1, lst[randomfile2])
                    if randomfile2 > 4 and randomfile2 < lenfile - (k + 1):
                        randomfile = randomfile2
            actionList[counter * k, :] = self.GetAction(randomfolder, randomfile)
            imglist[counter * k, :, :, 3] = self.GetImage(randomfolder, randomfile)
            imglist[counter * k, :, :, 2] = self.GetImage(randomfolder, randomfile - 1)
            imglist[counter * k, :, :, 1] = self.GetImage(randomfolder, randomfile - 2)
            imglist[counter * k, :, :, 0] = self.GetImage(randomfolder, randomfile - 3)
            outputseq[counter,0,:]=generator.PredictRepresentation([imglist[counter * k, :, :]], [actionList[counter * k, :]])
            sumrew=self.GetReward(randomfolder,randomfile)
            if rewconst.outputTime and sumrew==1:
                sumrew=0
            elif rewconst.outputTime:
                sumrew=-1
            if rewconst.isSequence and rewconst.Classification and not rewconst.outputTime:
                sumrewarr[counter,0]=sumrew
            for i in range(1, k):
                actionList[counter * k + i, :] = self.GetAction(randomfolder, randomfile + i)
                imglist[counter * k + i, :, :, 2] = imglist[counter * k + i - 1, :, :, 3]
                imglist[counter * k + i, :, :, 1] = imglist[counter * k + i - 1, :, :, 2]
                imglist[counter * k + i, :, :, 0] = imglist[counter * k + i - 1, :, :, 1]
                imglist[counter * k + i, :, :, 3] = np.reshape(
                    generator.PredictImage([imglist[counter * k + i - 1, :, :, :]], [actionList[counter * k + i - 1, :]]),
                    (constant.input_height, constant.input_width))
                #self.ShowImage(imglist[counter * k + i, :, :, 3]*255+self.mean)
                outputseq[counter, i, :] = generator.PredictRepresentation([imglist[counter * k, :, :]],
                                                                           [actionList[counter * k, :]])
                sumrewtemp=self.GetReward(randomfolder,randomfile+i)
                if rewconst.isSequence and rewconst.Classification and not rewconst.outputTime:
                    sumrewarr[counter, i] = sumrewtemp
                elif rewconst.outputTime and sumrewtemp>0:
                    sumrew=i
                if not rewconst.outputTime:
                    sumrew=sumrew+(0.5**i)*sumrewtemp

            if rewconst.Classification and not rewconst.isSequence and not rewconst.outputTime:
                if sumrew>0:
                    numberofRewardedBatch+=1
                    if rewconst.ThreeClasses and not rewconst.isSequence:
                        sumrewarr[counter,2]=1
                    else:
                        sumrewarr[counter]=1
                elif sumrew<0 and rewconst.ThreeClasses:
                    numberofPunishedBatch+=1
                    sumrewarr[counter,0]=1
                else:
                    if rewconst.ThreeClasses and not rewconst.isSequence:
                        sumrewarr[counter,1]=1
                    else:
                        sumrewarr[counter] = 0
            elif rewconst.Classification==False:
                if sumrew>0:
                    numberofRewardedBatch+=1
                #sumrew=math.exp(-np.logaddexp(0, -sumrew))
                #sumrew=np.log(sumrew+1e-10)
                sumrewarr[counter] = sumrew
            elif rewconst.outputTime:
                sumrewarr[counter]=sumrew
            counter = counter + 1
        return self.unison_shuffled_copies(outputseq,sumrewarr)

    def unison_shuffled_copies(self,a, b):
        assert len(a) == len(b)
        p = np.random.permutation(len(a))
        return a[p], b[p]
    def GetRewardFrame(self,episode,numbertosearchfor):
        rewlist = []
        if constant.OneEpisode==False:
            f=open(os.path.join(os.path.join(constant.DatasetAddress, episode), "rew.log"))
            nlines = 0
            for line in f:
                nlines += 1
                if (numbertosearchfor==1 and line.find("1.0") >= 0):
                    rewlist.append(nlines)
                elif ((numbertosearchfor==-1) and  line.find("-1")>0):
                    rewlist.append(nlines)
                elif line.find("0.0")>0:
                    rewlist.append(nlines)
        else:
            if numbertosearchfor==0:
                rewlist=self.rewzero
            elif numbertosearchfor==1:
                rewlist=self.rewpositive
            else:
                rewlist=self.rewnegetive


        return rewlist