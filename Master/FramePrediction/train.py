import sys
import os
sys.path.insert(0,os.getcwd()+"/FramePrediction/")
import network
import tensorflow as tf
import constant
import os
from dataset import Dataset
def StartTraining():
    graph=tf.Graph()
    with graph.as_default():
        sess = tf.Session()
        dataset=Dataset()

        lossPlot = tf.get_variable('loss_plot', shape=(), initializer=tf.constant_initializer(0.0),trainable=False)
        tf.summary.scalar("GeneratorLoss", lossPlot)
        lossPlotbase = tf.get_variable('loss_plot_base', shape=(), initializer=tf.constant_initializer(0.0), trainable=False)
        tf.summary.scalar("GeneratorBaseLoss", lossPlotbase)
        net=network.network(dataset,sess,True)
        currentNetwork=net
        batchsize=constant.firstbatchsize

        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
        merge = tf.summary.merge_all()
        writer = tf.summary.FileWriter(constant.LogAddress)

        epoch=int(constant.numberofTrainningEpoch)

        if (os.path.isfile(constant.SaveFileAddress+"model.ckpt.index")):
            saver.restore(sess, constant.SaveFileAddress+"model.ckpt")
        lasti=-1
        k=1
        for i in range(epoch):
            if i==constant.k1to3Switch:
                batchsize=constant.secondbatchsize
                k=3
                print("K Changed to 3")
            elif i==constant.k3to5switch:
                k=5
                print("K Changed to 5")
            imglist, outputlist, actionList = dataset.CreateMiniBatch(currentNetwork,batchsize,k)

            sess.run((currentNetwork.train), feed_dict={currentNetwork.x: imglist, currentNetwork.y_: outputlist, currentNetwork.action: actionList})

            if i % 3000 == 0 and i > 0:
                saver.save(sess, constant.SaveFileAddress+"model.ckpt")
                img, action, outimg, randomfolder, randomfile = dataset.GetRandomImage(True)
                print("folder= " + str(randomfolder) + " and file= " + str(randomfile))
                currentNetwork.PredictImageSequence(img, action, len(action))
                dataset.CreateOutput(randomfolder, randomfile)

            if int(i * 100 / epoch) > lasti:
                loss,baseloss = sess.run((currentNetwork.loss,currentNetwork.baseloss), feed_dict={currentNetwork.x: imglist, currentNetwork.y_: outputlist,
                                                                  currentNetwork.action: actionList})
                lasti = int(i * 100 / epoch)
                print(str(lasti) + " Percent Completed loss is equal to " + str(loss))
                sess.run(tf.assign(lossPlot, loss))
                sess.run(tf.assign(lossPlotbase, baseloss))
                writer.add_summary(sess.run(merge), i)

StartTraining()