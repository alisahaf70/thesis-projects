import generator
import GeneralFunction
import constant as constant
import tensorflow as tf
import shutil
import os
import numpy as np
from PIL import Image
import re

class network:
    def __init__(self,dataset,session,trainnetwork):
        #Create PlaceHolder Variables
        self.dataset=dataset
        self.sess=session
        self.action = tf.placeholder(dtype=tf.float32, shape=(None, constant.number_of_action),name="networkAction")
        self.x = tf.placeholder(dtype=tf.float32, shape=(None, constant.input_height, constant.input_width, constant.historyLength),name="networkinput")
        self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, constant.input_height, constant.input_width, 1),name="networklabel")
        self.CreateNetwork(trainnetwork)

    def CreateNetwork(self,trainnetwork):
        generator1=generator.Generator(1)
        generator1net,self.rep=generator1.Create_Network(self.action,self.x)
        self.out=generator1net
        if trainnetwork:
            return self.CreateOptimiser(generator1net)

    def CreateOptimiser(self,out):
        LearningRate = tf.Variable(1e-4, trainable=False)
        global_step = tf.get_variable('global_step', shape=(), initializer=tf.constant_initializer(0.0),
                                      trainable=False)
        learning_rate = tf.train.exponential_decay(LearningRate, global_step, 1e4, 0.9, staircase=True)
        optimizer = tf.train.AdamOptimizer(learning_rate)
        penalty = tf.reduce_sum(0 * tf.stack([tf.nn.l2_loss(var) for var in tf.trainable_variables()]),
                                name='regularization')
        self.loss,self.baseloss= GeneralFunction.combined_loss(out, self.y_)
        self.loss+=penalty



        grads_vars = optimizer.compute_gradients(self.loss)
        bias_pattern = re.compile('.*bias*')
        grads_vars_mult = []
        for grad, var in grads_vars:
            if bias_pattern.match(var.op.name):
                grads_vars_mult.append((grad * 2.0, var))
            else:
                grads_vars_mult.append((grad, var))

        grads_clip = [(tf.clip_by_value(grad, -0.1, 0.1), var) for grad, var in grads_vars_mult]
        self.train = optimizer.apply_gradients(grads_clip, global_step=global_step)
        return self.train

    def PredictImageSequence(self, img, actionList, step):
        step=min(200,step)
        shutil.rmtree(constant.OutputAddress)
        os.mkdir(constant.OutputAddress)
        imglist = np.zeros((constant.input_height, constant.input_width, step + constant.historyLength))
        imglist[:, :, 0:constant.historyLength] = img

        for i in range(step - constant.historyLength):
            imglist[:, :, constant.historyLength + i] = np.reshape(self.sess.run(self.out, feed_dict={self.x: [imglist[:, :, i:i + constant.historyLength]],
                                                                                                      self.action: [actionList[i, :]]}),
                                                                   newshape=(constant.input_height, constant.input_width))
            Image.fromarray((imglist[:, :, constant.historyLength + i] +  self.dataset.GetMean()/ 255) * 255).convert('RGB').save(
                constant.OutputAddress+"out" + str(i) + ".png")
        outfile = open(constant.OutputAddress+"act.log", "w+")
        ac = np.argmax(actionList, axis=1).tolist()
        outfile.writelines(list("%d\n" % item for item in ac))

    def PredictRepresentation(self, img, action):
        repout = self.sess.run(self.rep, feed_dict={self.x: img, self.action: action})
        return repout
    def PredictImage(self, img, action):
        imgout = self.sess.run(self.out, feed_dict={self.x: img, self.action: action})
        return imgout