import argparse
import gym
import os
import numpy as np
import cv2
import scipy.misc
import random

from ModelBased.agent import Agent
from ModelBased.agent import DQNAgent
from ModelBased.env.atari_wrappers_deprecated import wrap_dqn
from ModelBased.env.misc_util import SimpleMonitor

Episodebase=True
def parse_args():
    parser = argparse.ArgumentParser("Run an already learned DQN model.")
    # Environment
    parser.add_argument("--env", type=str, required=False   , help="name of the game",default="Freeway")
    parser.add_argument("--model-dir", type=str, default=None, help="load model from this directory. ")
    parser.add_argument("--video", type=str, default=None, help="Path to mp4 file where the video of first episode will be recorded.")

    return parser.parse_args()


def make_env(game_name):
    env = gym.make(game_name + "NoFrameskip-v4")
    env = SimpleMonitor(env)
    env = wrap_dqn(env)
    return env

def SaveImage(_screen,Path,Filename):
    if not os.path.exists(Path):
        os.makedirs(Path)
    scipy.misc.toimage(_screen).save(Path+Filename)
def play(env):
    average = np.zeros([84, 84], dtype=np.float64)
    action_repeat = 3
    episodeNumber = 0
    framenumber = 0
    skipfirst=True
    PATH = "/home/mll/@li/thesis-projects/Dataset/image/"
    savePATH = "/home/mll/@li/thesis-projects/Dataset/"
    if Episodebase:
        os.makedirs(PATH+'{:04d}'.format(0))
        f=open( PATH+'{:04d}'.format(0)+"/act.log", 'w')
        r=open( PATH+'{:04d}'.format(0)+"/rew.log", 'w')
    else:
        f = open(savePATH + "/act.log", 'w')
        r = open(savePATH + "/rew.log", 'w')
    obs = env.reset()
    ob=DQNAgent()
    for cc in range(100000):
        if cc%1000==0:
            print(str(cc)+"\n")
        health = env.unwrapped.ale.lives()
        #env.unwrapped.render()
        if random.random()<0.51:
            action,p,v=ob.GetBestAction(np.array(obs)[None])
            action=action[0]
        else:
            action=random.randint(0,env.action_space.n-1)
        #action = act(np.array(obs)[None], stochastic=stochastic)[0]
        obs, rew, done, info = env.step(action)
        _screen=obs._frames[3]
        _screen=np.reshape(_screen,newshape=(84,84))
        if (Episodebase and (health != env.unwrapped.ale.lives() or done==True)):
            episodeNumber += 1
            f.close()
            r.close()
            skipfirst=True
            os.makedirs(PATH + '{:04d}'.format(episodeNumber))
            f = open(PATH + '{:04d}'.format(episodeNumber) + "/act.log", 'w')
            r = open(PATH + '{:04d}'.format(episodeNumber) + "/rew.log", 'w')
            framenumber = 0
        elif health != env.unwrapped.ale.lives():
            rew=-1
        # _screen=_screen/255
        average = (average * cc + _screen) / (cc + 1)
        if Episodebase:
            SaveImage(_screen, PATH + '{:04d}'.format(episodeNumber) + "/", '{:05d}'.format(framenumber) + ".png")
        else:
            SaveImage(_screen, PATH , '{:06d}'.format(framenumber) + ".png")
        framenumber += 1
        if not skipfirst:
            f.write(str(action)+"\n")
            r.write(str(rew)+"\n")
        else:
            skipfirst=False
        if done:
            obs = env.reset()
            continue
    f.close()
    r.close()
    np.save("mean", average)


if __name__ == '__main__':
    args = parse_args()
    random.seed()
    env = make_env(args.env)
    play(env)