import os
import argparse
import numpy as np
import tensorflow as tf

MODEL_DIR = os.path.join("/home/mll/@li/thesis-projects/Master/FramePrediction/ExpertModel/games/Breakout/", 'model')
DQN_MODEL_DIR = os.path.join("/home/mll/@li/thesis-projects/Master/FramePrediction/ExpertModel/games/Freeway/", 'model')


class Agent:
    def __init__(self):
        g = tf.Graph()
        with g.as_default():
            self.sess = tf.Session()
            saver = tf.train.import_meta_graph(os.path.join(MODEL_DIR, 'model.meta'))
            saver.restore(self.sess, tf.train.latest_checkpoint(MODEL_DIR))
            self.input = g.get_tensor_by_name('global/Placeholder:0')
            self.pi = g.get_tensor_by_name('global/fully_connected_2/Softmax:0')
            self.v = g.get_tensor_by_name('global/fully_connected_1/BiasAdd:0')
            self.output = tf.argmax(self.pi, axis=1)

    def GetBestAction(self, x):
        a,p,v = self.sess.run((self.output,self.pi,self.v), feed_dict={self.input: x})
        return a,p,v
class DQNAgent:
    def __init__(self):
        g = tf.Graph()
        with g.as_default():
            self.sess = tf.Session()
            saver = tf.train.import_meta_graph(os.path.join(DQN_MODEL_DIR, 'model.meta'))
            saver.restore(self.sess, tf.train.latest_checkpoint(DQN_MODEL_DIR))
            self.input = g.get_tensor_by_name('main/Placeholder:0')
            self.outputq = g.get_tensor_by_name('main/fully_connected_1/BiasAdd:0')
            self.preq=g.get_tensor_by_name('main/fully_connected/BiasAdd:0')
            self.action = tf.argmax(self.outputq,axis=1)
    def GetBestAction(self, x,returnPre=False):
        a,q,l = self.sess.run((self.action,self.outputq,self.preq), feed_dict={self.input: x})
        if returnPre:
            return a, q, None,l
        return a,q,None