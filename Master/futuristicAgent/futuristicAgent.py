import numpy as np
import tensorflow as tf
import os
from futuristicAgent.env import make_env
import RewardPrediction.constant as rewconst
import futuristicAgent.constant as constant
class FuturisticAgent():
    def __init__(self,expert,framepredict,rewardpredict):
        g=tf.Graph()
        with g.as_default():
            self.sess=tf.Session()
            self.fp=framepredict
            self.rp=rewardpredict
            self.agent=expert
            self.CreateEnv()
            self.CreateNetwork()
            self.CreateOptimiser()
            self.rew = tf.get_variable('reward', shape=(), initializer=tf.constant_initializer(0.0), trainable=False)
            tf.summary.scalar("Loss", self.loss)
            tf.summary.scalar("RewardPerEpisode", self.rew)
            self.sess.run(tf.global_variables_initializer())
            self.saver = tf.train.Saver()
            self.merge = tf.summary.merge_all()
            self.writer = tf.summary.FileWriter(constant.LogAddress)
            if (os.path.isfile(constant.SaveFileAddress + "model.ckpt.index")):
                self.saver.restore(self.sess, constant.SaveFileAddress + "model.ckpt")

    def CreateFuturePrediction(self,image):
        outseq = self.fp.CreateRepresentationPredictionforAction(image, rewconst.Lstm_cell)
        outrewtemp = self.rp.GetPrediction(outseq)
        if rewconst.ThreeClasses:
            outrew = np.reshape(outrewtemp, newshape=(np.array(image).shape[0], self.env.action_space.n* 3))
            return outrew
        else:
            outrew = np.reshape(outrewtemp, newshape=(np.array(image).shape[0], self.env.action_space.n, 1))
            return np.squeeze(outrew, axis=(2))



    def CreateNetwork(self):
        LinearInitializer = tf.contrib.layers.xavier_initializer(uniform=True)
        BiasInitializer = tf.constant_initializer(0.0)
        self.inputV=tf.placeholder(dtype=tf.float32,shape=(None),name="V")
        self.inputP=tf.placeholder(dtype=tf.float32,shape=(None,self.env.action_space.n),name="P")
        if rewconst.ThreeClasses:
            self.inputFuture=tf.placeholder(dtype=tf.float32,shape=(None,self.env.action_space.n*3),name="future")
            wflat1 = tf.get_variable("futuristic/wflat1", shape=(self.env.action_space.n * 4, 64),
                                     initializer=LinearInitializer)
        else:
            self.inputFuture=tf.placeholder(dtype=tf.float32,shape=(None,self.env.action_space.n),name="future")
            wflat1 = tf.get_variable("futuristic/wflat1", shape=(self.env.action_space.n * 2, 64),
                                     initializer=LinearInitializer)
        self.R = tf.placeholder(tf.float32, [None])
        self.a = tf.placeholder(tf.uint8, [None])
        self.a_one_hot = tf.one_hot(self.a, self.env.action_space.n, dtype=tf.float32)



        bflat1 = tf.get_variable("futuristic/bflat1", shape=64, initializer=BiasInitializer)
        flat1=tf.nn.relu(tf.matmul(tf.concat((self.inputP,self.inputFuture),axis=1,name="concat"),wflat1,name="zarb")+bflat1,name="relu")

        wflat2 = tf.get_variable("futuristic/wflat2", shape=(64,self.env.action_space.n),
                                 initializer=LinearInitializer)
        bflat2 = tf.get_variable("futuristic/bflat2", shape=self.env.action_space.n, initializer=BiasInitializer)
        self.flat2 = tf.matmul(flat1, wflat2,name="zarb2") + bflat2
        self.prob=tf.nn.softmax(self.flat2)

    def CreateEnv(self):
        self.env = make_env("Breakout")
    def Train(self):
        s = self.env.reset()
        s=np.array(s, copy=False)
        for t in range(constant.TrainningEpoch):
            experience_t =0
            s_list = []
            a_list = []
            r_list = []
            v_list = []
            f_list = []
            p_list = []
            while True:
                s_list.append(s)
                _,p,[[v]]=self.agent.GetBestAction([s])
                [future]=self.CreateFuturePrediction([s])
                [p] = self.sess.run(self.prob, feed_dict={self.inputP:p,self.inputFuture:[future],self.inputV:[v]})
                a = np.random.choice(np.arange(0, self.env.action_space.n), p=p)

                v_list.append(v)
                f_list.append(future)
                p_list.append(p)
                a_list.append(a)

                s, r, d, i = self.env.step(a)
                if d:
                    s = self.env.reset()
                s = np.array(s, copy=False)
                r_list.append(r)
                experience_t += 1
                if d or experience_t == constant.T_MAX:
                    break
            R = 0
            if not d:
                R = v
            n = len(s_list)
            R_list = []
            for i in reversed(range(n)):
                R = constant.DISCOUNT * R + r_list[i]
                R_list.append(float(R))
            R_list = R_list[::-1]
            sumin = np.sum(R_list)
            self.sess.run(tf.assign(self.rew, sumin))
            _,loss,mrg=self.sess.run((self.train,self.loss,self.merge),feed_dict={self.R:R_list,self.inputV:v_list,self.inputP:p_list,self.inputFuture:f_list,self.a:a_list})
            self.writer.add_summary(mrg, t)
            print("{:06.2f}  Percent Completed loss is equal to {:06.2f} and reward is {:06.2f}".format((t*100/constant.TrainningEpoch),loss,sumin))

            if t % 3000 == 0 and t > 1:
                self.saver.save(self.sess, constant.SaveFileAddress+"model.ckpt")



    def CreateOptimiser(self):
        self.p = tf.reduce_sum(self.prob * self.a_one_hot, axis=1)
        self.p_loss = -tf.log(self.p + 1E-6) * (self.R - self.inputV)
        self.h_loss = 0.01 * tf.reduce_sum(tf.log(self.prob + 1E-6) * self.prob, axis=1)
        self.loss = tf.reduce_sum(self.p_loss + self.h_loss)

        optimiser = tf.train.RMSPropOptimizer(3E-4, 0.99, 0, 0.1)
        grads_vars = optimiser.compute_gradients(self.loss)
        grads_clip = [(tf.clip_by_average_norm(grad, 40), var) for grad, var in grads_vars]
        self.train = optimiser.apply_gradients(grads_clip)
        return self.train
