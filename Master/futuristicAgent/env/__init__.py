import gym
from futuristicAgent.env.misc_util import SimpleMonitor
from futuristicAgent.env.atari_wrappers_deprecated import wrap_dqn


def make_env(game_name, monitor_dir=None):
    env = gym.make(game_name + "NoFrameskip-v4")
    if monitor_dir:
        env = gym.wrappers.Monitor(env, directory=monitor_dir, force=True)
    env = SimpleMonitor(env)  # puts rewards and number of steps in info, before environment is wrapped
    env = wrap_dqn(env)  # applies a bunch of modification to simplify the observation space (downsample, make b/w)
    return env
