import RewardPrediction.network as network
import tensorflow as tf
import RewardPrediction.constant as constant
import os
import FramePrediction.run as fprun

def StartTraining():
    graph=tf.Graph()
    with graph.as_default():
        sess = tf.Session()

        frameprediction=fprun.FramePrediction()
        curnetwork=network.network(sess)
        out, sigout=curnetwork.CreateNetwork()
        optimizer,loss=curnetwork.CreateOptimiser()
        tf.summary.scalar("RewardLoss", loss)
        batchsize=constant.batchsize

        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
        merge = tf.summary.merge_all()
        writer = tf.summary.FileWriter(constant.LogAddress)

        epoch=int(constant.numberofTrainningEpoch)
        lasti=-1
        if (os.path.isfile(constant.SaveFileAddress+"model.ckpt.index")):
            saver.restore(sess, constant.SaveFileAddress+"model.ckpt")

        for i in range(constant.numberofTrainningEpoch):
            imglist, rew=frameprediction.CreateTrainingsetforReward(batchsize,constant.Lstm_cell)

            sess.run(optimizer, feed_dict={curnetwork.x: imglist, curnetwork.y_: rew})
            if i % 300 == 0 and i > 1:
                saver.save(sess, constant.SaveFileAddress+"model.ckpt")

            if True or int(i * 100 / constant.numberofTrainningEpoch) > lasti:
                lossvalue,mergevalue = sess.run((loss,merge), feed_dict={curnetwork.x: imglist, curnetwork.y_: rew})
                lasti = int(i * 100 / epoch)
                print(str(lasti) + " Percent Completed loss is equal to " + str(lossvalue))
                writer.add_summary(mergevalue, i)
            if i%100==0:
                outnet = sess.run((curnetwork.sigout), feed_dict={curnetwork.x: imglist, curnetwork.y_: rew})

StartTraining()