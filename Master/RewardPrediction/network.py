from RewardPrediction.GeneralFunction import selu
import RewardPrediction.constant as constant
import numpy as np
import tensorflow as tf

class network:
    def __init__(self,session):
        #Create PlaceHolder Variables
        self.sess=session
        self.x = tf.placeholder(dtype=tf.float32, shape=(None, constant.Lstm_cell, 1024),name="networkinput")
        if constant.Classification and constant.isSequence:
            self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, constant.Lstm_cell), name="networklabel")
        elif constant.ThreeClasses:
            self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, 3),name="networklabel")
        else:
            self.y_ = tf.placeholder(dtype=tf.float32, shape=(None, 1), name="networklabel")


    def CreateNetwork(self):
        LinearInitializer = tf.contrib.layers.xavier_initializer(uniform=True)
        BiasInitializer = tf.constant_initializer(0.0)
        shape = tf.shape(self.x)
        dim = tf.reduce_prod(shape[:2])
        x = tf.reshape(self.x, [dim,1024])


        wflat0 = tf.get_variable("RewardPrediction/wflat0", shape=(1024, 1024), initializer=LinearInitializer)
        bflat0 = tf.get_variable("RewardPrediction/bflat0", shape=1024, initializer=BiasInitializer)

        l0flat = selu(tf.matmul(x, wflat0) + bflat0)
        x=tf.reshape(l0flat,[shape[0],shape[1],1024])
        x = tf.unstack(x, constant.Lstm_cell, 1)


        LSTMHiddenSize=128
        if constant.isSequence:
            LSTMHiddenSize=1024
        # Define a lstm cell with tensorflow
        lstm_cell = tf.contrib.rnn.LayerNormBasicLSTMCell(LSTMHiddenSize, forget_bias=1.0, dropout_keep_prob=0.9)
        #lstm_cell2 = tf.contrib.rnn.LayerNormBasicLSTMCell(LSTMHiddenSize, forget_bias=1.0, dropout_keep_prob=0.9)
        #lstm_cell = tf.contrib.rnn.MultiRNNCell([lstm_cell,lstm_cell2])
        # Get lstm cell output
        outputs, states = tf.contrib.rnn.static_rnn(lstm_cell, x, dtype=tf.float32)
        if constant.Classification and constant.isSequence:
            encoder_state = tf.unstack(states, axis=0)
            #encoder_state=encoder_state[0]
            with tf.variable_scope("deconv"):
                decoder = tf.contrib.rnn.LayerNormBasicLSTMCell(LSTMHiddenSize, forget_bias=1.0, dropout_keep_prob=0.9)
            #y=tf.concat((tf.zeros([tf.shape(self.y_)[0],1]),self.y_),axis=1)
                # y = tf.unstack(tf.reshape(tf.concat((self.y_,tf.zeros((tf.shape(self.y_)[0],1))),axis=1),[tf.shape(self.y_)[0],tf.shape(self.y_)[1]+1,1]), constant.Lstm_cell+1, 1)
                y = tf.unstack(tf.zeros((tf.shape(self.y_)[0],tf.shape(self.y_)[1]+1,1)), constant.Lstm_cell+1, 1)

                decoutputs, decstates = tf.contrib.rnn.static_rnn(decoder, y, dtype=tf.float32,initial_state=encoder_state)

                wflat1 = tf.get_variable("RewardPrediction/wflat1", shape=(LSTMHiddenSize, 128),
                                         initializer=LinearInitializer)
                bflat1 = tf.get_variable("RewardPrediction/bflat1", shape=128, initializer=BiasInitializer)


                shapes = tf.shape(decoutputs)
                decoutputs=tf.transpose(decoutputs, perm=[1, 0, 2])
                decoutputs=tf.reshape(decoutputs,(shapes[0]*shapes[1],shapes[2]))
                # self.sss=decoutputs
                l1flat = selu(tf.matmul(decoutputs, wflat1) + bflat1)
                wflat2 = tf.get_variable("RewardPrediction/wflat2", shape=(128, 1), initializer=LinearInitializer)
                bflat2 = tf.get_variable("RewardPrediction/bflat2", shape=1, initializer=BiasInitializer)
                l2flat = tf.matmul(l1flat, wflat2) + bflat2
                l2flat = tf.reshape(l2flat,(tf.shape(self.y_)[0] , constant.Lstm_cell+1))
                self.sigout=tf.nn.softmax(l2flat)
                self.out=l2flat
        elif constant.Classification:
            wflat1=tf.get_variable("RewardPrediction/wflat1",shape=(LSTMHiddenSize,32),initializer=LinearInitializer)
            bflat1=tf.get_variable("RewardPrediction/bflat1",shape=32,initializer=BiasInitializer)

            l1flat=selu(tf.matmul(outputs[-1],wflat1)+bflat1)
            if constant.ThreeClasses:
                wflat2 = tf.get_variable("RewardPrediction/wflat2", shape=(32, 3), initializer=LinearInitializer)
                bflat2 = tf.get_variable("RewardPrediction/bflat2", shape=3, initializer=BiasInitializer)
                self.out = tf.matmul(l1flat, wflat2) + bflat2
                self.sigout = tf.nn.softmax(self.out)
            elif constant.outputTime:
                wflat2 = tf.get_variable("RewardPrediction/wflat2", shape=(32, 1), initializer=LinearInitializer)
                bflat2 = tf.get_variable("RewardPrediction/bflat2", shape=1, initializer=BiasInitializer)
                self.out = tf.nn.relu(tf.matmul(l1flat, wflat2) + bflat2)
                self.sigout = self.out
            else:
                wflat2 = tf.get_variable("RewardPrediction/wflat2", shape=(32, 1), initializer=LinearInitializer)
                bflat2 = tf.get_variable("RewardPrediction/bflat2", shape=1, initializer=BiasInitializer)
                self.out = tf.matmul(l1flat, wflat2) + bflat2
                self.sigout = tf.nn.sigmoid(self.out)
        else:
            wflat1 = tf.get_variable("RewardPrediction/wflat1", shape=(LSTMHiddenSize, 32), initializer=LinearInitializer)
            bflat1 = tf.get_variable("RewardPrediction/bflat1", shape=32, initializer=BiasInitializer)

            l1flat = selu(tf.matmul(outputs[-1], wflat1) + bflat1)

            #wflat2 = tf.get_variable("RewardPrediction/wflat2", shape=(256, 32), initializer=LinearInitializer)
            #bflat2 = tf.get_variable("RewardPrediction/bflat2", shape=32, initializer=BiasInitializer)

            #l2flat = selu(tf.matmul(l1flat, wflat2) + bflat2)

            wflat3 = tf.get_variable("RewardPrediction/wflat3", shape=(32, 1), initializer=LinearInitializer)
            bflat3 = tf.get_variable("RewardPrediction/bflat3", shape=1, initializer=BiasInitializer)

            l3flat = tf.matmul(l1flat,wflat3) + bflat3
            self.out = l3flat
            self.sigout=l3flat
        return self.out,self.sigout


    def CreateOptimiser(self):
        # Define loss and optimizer
        if constant.Classification==False:
            cost = tf.reduce_sum(tf.squared_difference(self.out,self.y_))
            self.loss = cost
        elif constant.isSequence:
            cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.out, labels=tf.concat((tf.zeros((tf.shape(self.y_)[0],1)),self.y_),axis=1)))
            self.loss = cost
        elif constant.ThreeClasses:
            cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.out, labels=self.y_))
            self.loss = cost
        elif constant.outputTime==True:
            cost = tf.reduce_mean(tf.squared_difference(self.out,self.y_))
            self.loss = tf.reduce_mean(tf.squared_difference(tf.round(self.out), self.y_))
        else:
            cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.out, labels=self.y_))
            self.loss = tf.reduce_mean(tf.squared_difference(tf.round(self.sigout), self.y_))
            self.loss=cost
        self.optimizer = tf.train.AdamOptimizer(learning_rate=1e-4)

        #grads_vars = self.optimizer.compute_gradients(self.loss)
        #lst=[]
        #for grad,vars in grads_vars:
        #    lst.append(tf.reduce_mean(grad))
        #bb=tf.reduce_mean(lst)
        gvs = self.optimizer.compute_gradients(cost)
        capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs]
        self.optimizer = self.optimizer.apply_gradients(capped_gvs)
        # self.optimizer=self.optimizer.minimize(cost)
        return self.optimizer,self.loss
