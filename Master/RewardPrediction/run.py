import RewardPrediction.network as network
import tensorflow as tf
import RewardPrediction.constant as constant
from FramePrediction.run import FramePrediction as FramePredictionn
import os
import numpy as np

class RewardPrediction:
    def __init__(self):
        self.saf=None
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.sess = tf.Session()
            self.curnetwork = network.network(self.sess)
            out, self.sigout = self.curnetwork.CreateNetwork()

            self.sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()

            if (os.path.isfile(constant.SaveFileAddress + "model.ckpt.index")):
                saver.restore(self.sess, constant.SaveFileAddress + "model.ckpt")
            else:
                print("Can not Load RewardPRediction Save File")
                exit(0)
    def GetPrediction(self,ImageList):
        #1 mosavie lstm cell bud
        return self.sess.run(self.sigout,feed_dict={self.curnetwork.x:ImageList,self.curnetwork.y_:np.zeros((ImageList.shape[0],1))})

    def GetPerFramePrediction(self):
        fp=FramePredictionn()
        for ii in range(0,1):
            img, action, outimg, randomfolder, randomfile = fp.dataset.GetRandomImage(True,True)
            print("folder= " + str(randomfolder) + " and file= " + str(randomfile))
            fp.predictor.PredictImageSequence(img, action, len(action))
            fp.dataset.CreateOutput(randomfolder,randomfile)
            action=action[:40]
            repr=fp.GetPerFramePrediction(img,40,action)
            output=np.zeros((20))
            for i in range(constant.Lstm_cell):
                output[i]=self.GetPrediction(np.asarray([repr[i:20+i]]))
            if np.max(output)>0.9:
                for iii in range(len(output)):
                    if output[iii]>0.97:
                        last=iii
                print('reward is in ' + str(last) + ' sequence')
            print ("===================")
    def GetRewardTimeForAction(self,image,action,fraepredictor,lastaction):
        if self.saf==None:
            self.lastImage=image
            b=fraepredictor.GetRepresentation(image,fraepredictor.convertToOneHot([0]))
            self.saf=np.ones((20,1024))*b
        last=1000
        fp = fraepredictor

        repr = np.vstack((self.saf[1:],fp.GetPerFramePredictionforingame(image, 21, action)))
        if action==1:
            newimage = fp.GetRepresentation(self.lastImage, fp.convertToOneHot([lastaction]))
            self.saf = np.roll(self.saf, -1)
            self.saf[19]=newimage
            self.lastImage=image
        output = np.zeros((20))
        for i in range(constant.Lstm_cell):
            output[i] = self.GetPrediction(np.asarray([repr[i:20 + i]]))
        if np.max(output) > 0.9:
            for iii in range(len(output)):
                if output[iii] > 0.97:
                    last = iii
        return last
