import gym
import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import os
import pickle

PATH="/home/mll/@li/thesis-projects/Predict-frame/image/"
SavetoDisk=True

def ShowImage(_screen):
    imgplot = plt.imshow(_screen, cmap='gray')
    plt.show()
def SaveImage(_screen,Path,Filename):
    if not os.path.exists(Path):
        os.makedirs(Path)
    scipy.misc.toimage(_screen).save(Path+Filename)

average=np.zeros([84,84],dtype=np.float64)
env = gym.make('Breakout-v0')
env.reset()
episodeNumber=0
framenumber=0
action_repeat=3
skipFirst=True
if (SavetoDisk==True):
    os.makedirs(PATH+'{:04d}'.format(0))
    f=open( PATH+'{:04d}'.format(0)+"/act.log", 'w')
    for cc in range(500000):
        health=env.unwrapped.ale.lives()
        #env.render()
        action=env.action_space.sample()
        _screen, reward,terminal, _=env.step(action)
        if(health!=env.unwrapped.ale.lives() or terminal==True):
            episodeNumber+=1
            skipFirst=True
            f.close()
            os.makedirs(PATH+'{:04d}'.format(episodeNumber))
            f=open( PATH+'{:04d}'.format(episodeNumber)+"/act.log", 'w')
            framenumber=0
        _screen=cv2.resize(np.dot(_screen[...,:3], [0.299, 0.587, 0.114]), (84,84))
        #_screen=_screen/255
        average=(average*cc+_screen)/(cc+1);
        SaveImage(_screen, PATH+'{:04d}'.format(episodeNumber)+"/", '{:05d}'.format(framenumber)+".png")
        framenumber+=1
        if cc%1000==0:
        	print(str(cc))
        if not skipFirst:
            f.write(str(action)+"\n")
        else:
            skipFirst=False
        #print >> f, action
        if terminal==True:
            env.reset()
            continue
        for _ in range(action_repeat):
            env.step(action)
    f.close()
    np.save("mean",average)
else:
    images=[[]]
    actions=[[]]
    tempimage=[]
    tempaction=[]
    for cc in range(500000):
        if cc%1000==0:
            print(str(cc)+"\n")

        health = env.unwrapped.ale.lives()
        #env.render()
        action = env.action_space.sample()
        _screen, reward, terminal, _ = env.step(action)
        if (health != env.unwrapped.ale.lives() or terminal==True):
            skipFirst = True
            images.append(tempimage)
            actions.append(tempaction)
            tempimage=[]
            tempaction=[]
        _screen = cv2.resize(np.dot(_screen[..., :3], [0.299, 0.587, 0.114]), (84, 84))
        # _screen=_screen/255
        average = (average * cc + _screen) / (cc + 1)
        tempimage.append(_screen)
        if not skipFirst:
            tempaction.append(action)
        else:
            skipFirst=False

        if terminal==True:
            env.reset()
            continue
        for _ in range(action_repeat):
            env.step(action)
    np.save("mean", average)
    del images[0]
    del actions[0]
    with open('images', 'wb') as fp:
        pickle.dump(images, fp)
    with open('actions', 'wb') as fp:
        pickle.dump(actions, fp)
