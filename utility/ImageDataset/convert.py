import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import os
import pickle
from scipy import misc
from PIL import Image

PATH="/home/mll/@li/thesis-projects/utility/ImageDataset/example/train/"
SavePATH="/home/mll/@li/thesis-projects/Predict-frame/image/"
SavetoDisk=True

def ShowImage(_screen):
    imgplot = plt.imshow(_screen, cmap='gray')
    plt.show()
def SaveImage(_screen,Path,Filename):
    if not os.path.exists(Path):
        os.makedirs(Path)
    scipy.misc.toimage(_screen).save(Path+Filename)

average=np.zeros([84,84],dtype=np.float64)

images=[[]]
actions=[[]]
tempimage=[]
tempaction=[]
episodes = os.listdir(PATH)
cc=0
for episode in episodes:
    filelist = os.listdir(os.path.join(PATH, episode))
    for frame in filelist:
        if frame=="act.log":
            continue
        _screen=misc.imread(os.path.join(os.path.join(PATH, episode),frame))
        _screen = cv2.resize(np.dot(_screen[..., :3], [0.299, 0.587, 0.114]), (84, 84))
        average = (average * cc + _screen) / (cc + 1)
        cc+=1;
        if SavetoDisk==True:
            SaveImage(_screen, SavePATH + episode + "/",frame)
        else:
            tempimage.append(_screen)
    actionListfile = open(os.path.join(os.path.join(PATH, episode), "act.log"))
    if SavetoDisk:
        lines = actionListfile.readlines()
        with open(os.path.join(os.path.join(SavePATH, episode), "act.log"), "w+") as f1:
            f1.writelines(lines)
    else:
        actions.append([int(i) for i in actionListfile.readlines()])
        images.append(tempimage)
        tempimage=[]
    actionListfile.close()
np.save("mean", average)
ShowImage(average*255)
if SavetoDisk==False:
    del images[0]
    del actions[0]
    with open('images', 'wb') as fp:
        pickle.dump(images, fp)
    with open('actions', 'wb') as fp:
        pickle.dump(actions, fp)
