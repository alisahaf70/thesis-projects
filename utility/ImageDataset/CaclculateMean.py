import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import os
import pickle
from scipy import misc
from PIL import Image
PATH="/home/mll/@li/thesis-projects/Predict-frame/image/"
average=np.zeros([84,84],dtype=np.float64)
episodes = os.listdir(PATH)
cc=0
for episode in episodes:
    filelist = os.listdir(os.path.join(PATH, episode))
    for frame in filelist:
        if frame=="act.log":
            continue
        _screen=misc.imread(os.path.join(os.path.join(PATH, episode),frame))
        average = (average * cc + _screen) / (cc + 1)
        cc+=1;
np.save("mean", average)