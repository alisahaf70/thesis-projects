import argparse
import gym
import os
import numpy as np
import cv2
import scipy.misc
import baselines.common.tf_util as U

from baselines import deepq
from baselines.common.misc_util import (
    boolean_flag,
    SimpleMonitor,
)
from baselines.common.atari_wrappers_deprecated import wrap_dqn
from baselines.deepq.experiments.atari.model import model, dueling_model


def parse_args():
    parser = argparse.ArgumentParser("Run an already learned DQN model.")
    # Environment
    parser.add_argument("--env", type=str, required=True, help="name of the game")
    parser.add_argument("--model-dir", type=str, default=None, help="load model from this directory. ")
    parser.add_argument("--video", type=str, default=None, help="Path to mp4 file where the video of first episode will be recorded.")
    boolean_flag(parser, "stochastic", default=True, help="whether or not to use stochastic actions according to models eps value")
    boolean_flag(parser, "dueling", default=False, help="whether or not to use dueling model")

    return parser.parse_args()


def make_env(game_name):
    env = gym.make(game_name + "NoFrameskip-v4")
    env = SimpleMonitor(env)
    env = wrap_dqn(env)
    return env

def SaveImage(_screen,Path,Filename):
    if not os.path.exists(Path):
        os.makedirs(Path)
    scipy.misc.toimage(_screen).save(Path+Filename)
def play(env, act, stochastic, video_path):
    average = np.zeros([84, 84], dtype=np.float64)
    action_repeat = 3
    episodeNumber = 0
    framenumber = 0
    skipfirst=True
    PATH = "/home/mll/@li/thesis-projects/Predict-frame/image/"
    os.makedirs(PATH+'{:04d}'.format(0))
    f=open( PATH+'{:04d}'.format(0)+"/act.log", 'w')
    r=open( PATH+'{:04d}'.format(0)+"/rew.log", 'w')
    obs = env.reset()
    for cc in range(500000):
        if cc%1000==0:
            print(str(cc)+"\n")
        health = env.unwrapped.ale.lives()
        #env.unwrapped.render()
        action = act(np.array(obs)[None], stochastic=stochastic)[0]
        obs, rew, done, info = env.step(action)
        _screen=obs._frames[3]
        _screen=np.reshape(_screen,newshape=(84,84))
        if (health != env.unwrapped.ale.lives() or done==True):
            episodeNumber += 1
            f.close()
            r.close()
            skipfirst=True
            os.makedirs(PATH + '{:04d}'.format(episodeNumber))
            f = open(PATH + '{:04d}'.format(episodeNumber) + "/act.log", 'w')
            r = open(PATH + '{:04d}'.format(episodeNumber) + "/rew.log", 'w')
            framenumber = 0
        # _screen=_screen/255
        average = (average * cc + _screen) / (cc + 1)
        SaveImage(_screen, PATH + '{:04d}'.format(episodeNumber) + "/", '{:05d}'.format(framenumber) + ".png")
        framenumber += 1
        if not skipfirst:
            f.write(str(action)+"\n")
            r.write(str(rew)+"\n")
        else:
            skipfirst=False
        if done:
            obs = env.reset()
            continue
    f.close()
    r.close()
    np.save("mean", average)


if __name__ == '__main__':
    with U.make_session(4) as sess:
        args = parse_args()
        env = make_env(args.env)
        act = deepq.build_act(
            make_obs_ph=lambda name: U.Uint8Input(env.observation_space.shape, name=name),
            q_func=dueling_model if args.dueling else model,
            num_actions=env.action_space.n)
        U.load_state(os.path.join(args.model_dir, "saved"))
        play(env, act, args.stochastic, args.video)